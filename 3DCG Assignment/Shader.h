#ifndef SHADER_H
#define SHADER_H

#include <d3d11.h>
#include <d3dcompiler.h>
#include <string>
#include "resource.h"
#include "D3D11Graphics.h"

class Shader
{
public:
	virtual void SetActive(D3D11Graphics* graphics) = 0;
	virtual void CleanUp() = 0;
	static HRESULT CompileShaderFromFile(LPCWSTR szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut);
};

#endif