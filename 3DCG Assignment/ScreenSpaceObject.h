#ifndef SCREENSPACEOBJECT_H
#define SCREENSPACEOBJECT_H

#include "GameObject.h"

class ScreenSpaceObject : public GameObject
{
public:
	ScreenSpaceObject(D3D11Graphics* graphics);
	HRESULT Initialise(DirectX::XMFLOAT2 Position, float size, std::string shaderFile = "Shaders/ScreenSpaceShader.fx", float depth = 0.0f);
	void SetShadersAndBuffers();
	void SetShaderResources();
	void SetShaderResources(ID3D11ShaderResourceView* positions, ID3D11ShaderResourceView* normals, ID3D11ShaderResourceView* colours);
	void Render();
	void CleanUp();
	void CreateBlendState();
	HRESULT SetTexture(std::string textureFileName);
	void SetTexture(ID3D11ShaderResourceView* texture);

private:
	ID3D11ShaderResourceView* texture = NULL;
	ID3D11BlendState* Transparency;
};

#endif