#ifndef RENDERTARGET_H
#define RENDERTARGET_H

#include <d3d11.h>
#include <d3dcompiler.h>
#include <directxmath.h>

class RenderTarget{
public:
	HRESULT Initialise(ID3D11Device* device, float width, float height);
	ID3D11Texture2D* getTexture();
	ID3D11RenderTargetView* getRenderTarget();
	ID3D11ShaderResourceView* getShaderResourceView();
	void ClearShaderResourceView(ID3D11Device* device);
	void CleanUp();
private:
	ID3D11Texture2D* RenderedTexture = NULL;
	ID3D11RenderTargetView* RenderTargetView = NULL;
	ID3D11ShaderResourceView* ShaderResourceView = NULL;
	D3D11_TEXTURE2D_DESC textureDesc;
	D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
};
#endif