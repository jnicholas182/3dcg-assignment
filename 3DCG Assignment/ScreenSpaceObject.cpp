#include <fstream>
#include "ScreenSpaceObject.h"
#include "VertexShader.h"
#include "PixelShader.h"
#include "GeometryShader.h"
#include "D3D11Graphics.h"
#include "WICTextureLoader.h"

using namespace DirectX;

//--------------------------------------------------------------------------------------
// Call base constructor for Game Object
//--------------------------------------------------------------------------------------
ScreenSpaceObject::ScreenSpaceObject(D3D11Graphics* graphics) : GameObject(graphics){}

//--------------------------------------------------------------------------------------
// Set up additional resources for Screen Space object (Vertices, vertex layout)
//--------------------------------------------------------------------------------------
HRESULT ScreenSpaceObject::Initialise(XMFLOAT2 Position, float size, std::string shaderFile, float depth)
{
	//float xSize = size *((float)graphics->height / (float)graphics->width);

	vertices.push_back(Vertex{ XMFLOAT3(Position.x - size, Position.y + size, depth), XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT2(0.0f, 0.0f) });
	vertices.push_back(Vertex{ XMFLOAT3(Position.x + size, Position.y + size, depth), XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT2(1.0f, 0.0f) });
	vertices.push_back(Vertex{ XMFLOAT3(Position.x - size, Position.y - size, depth), XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT2(0.0f, 1.0f) });
	vertices.push_back(Vertex{ XMFLOAT3(Position.x + size, Position.y - size, depth), XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT2(1.0f, 1.0f) });

	indices.push_back(0); indices.push_back(1); indices.push_back(3);
	indices.push_back(0); indices.push_back(3); indices.push_back(2);

	shaderFileName = std::wstring(shaderFile.begin(), shaderFile.end());

	HRESULT hr;

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	vertexShader = new VertexShader();
	vertexShader->Initialise(shaderFileName.c_str(), graphics, layout, ARRAYSIZE(layout));

	pixelShader = new PixelShader();
	pixelShader->Initialise(shaderFileName.c_str(), graphics);

	hr = BufferFactory::CreateVertexBuffer(graphics, vertices, &vertexBuffer);
	if (FAILED(hr))
		return hr;

	hr = BufferFactory::CreateIndexBuffer(graphics, indices, &indexBuffer);
	if (FAILED(hr))
		return hr;

	CreateBlendState();

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Set additional resources to be active (shaders, textures)
//--------------------------------------------------------------------------------------
HRESULT ScreenSpaceObject::SetTexture(std::string textureFileName)
{
	std::string t = "Models/ScreenSpaceTextures/" + textureFileName;
	std::ifstream infile(t);
	if (!infile.good())
		return E_FAIL;
	if (FAILED(CreateWICTextureFromFile(graphics->getDevice(), graphics->getContext(), std::wstring(t.begin(), t.end()).c_str(), nullptr, &texture)))
		return E_FAIL;
	return S_OK;
}

//--------------------------------------------------------------------------------------
// Set texture for screen space object
//--------------------------------------------------------------------------------------
void ScreenSpaceObject::SetTexture(ID3D11ShaderResourceView* texture)
{
	this->texture = texture;
}

//--------------------------------------------------------------------------------------
// Set additional resources to be active (shaders, textures)
//--------------------------------------------------------------------------------------
void ScreenSpaceObject::SetShadersAndBuffers()
{
	vertexShader->SetActive(graphics);
	graphics->getContext()->HSSetShader(NULL, NULL, 0);
	graphics->getContext()->DSSetShader(NULL, NULL, 0);
	graphics->getContext()->GSSetShader(NULL, NULL, 0);
	pixelShader->SetActive(graphics);

	SetVertexBuffer();
	SetIndexBuffer();
}

//--------------------------------------------------------------------------------------
// Set texture for shader for regular screen space object
//--------------------------------------------------------------------------------------
void ScreenSpaceObject::SetShaderResources()
{
	graphics->getContext()->PSSetShaderResources(5, 1, &texture);
}

//--------------------------------------------------------------------------------------
// Set textures for deferred shading second pass final quad
//--------------------------------------------------------------------------------------
void ScreenSpaceObject::SetShaderResources(ID3D11ShaderResourceView* positions, ID3D11ShaderResourceView* normals, ID3D11ShaderResourceView* colours)
{
	graphics->getContext()->PSSetShaderResources(6, 1, &positions);
	graphics->getContext()->PSSetShaderResources(7, 1, &normals);
	graphics->getContext()->PSSetShaderResources(8, 1, &colours);
}

//--------------------------------------------------------------------------------------
// Set topology and sampler for object and draw
//--------------------------------------------------------------------------------------
void ScreenSpaceObject::Render()
{
	float blendFactor[4] = { 0.75f, 0.75f, 0.75f, 0.0f };
	UINT sampleMask = 0xffffffff;

	graphics->getContext()->OMSetBlendState(Transparency, blendFactor, sampleMask);

	graphics->getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	graphics->SetSamplerLinear();

	graphics->getContext()->DrawIndexed(indices.size(), 0, 0);
}

//--------------------------------------------------------------------------------------
// Clean up resources
//--------------------------------------------------------------------------------------
void ScreenSpaceObject::CleanUp()
{
	GameObject::CleanUp();
	Transparency->Release();
	texture->Release();
}

//--------------------------------------------------------------------------------------
// Create transparency blend state for textures that have transparent pixels
//--------------------------------------------------------------------------------------
void ScreenSpaceObject::CreateBlendState()
{
	D3D11_BLEND_DESC blendDesc;
	ZeroMemory(&blendDesc, sizeof(blendDesc));

	D3D11_RENDER_TARGET_BLEND_DESC rtbd;
	ZeroMemory(&rtbd, sizeof(rtbd));

	rtbd.BlendEnable = TRUE;
	rtbd.SrcBlend = D3D11_BLEND_SRC_ALPHA;
	rtbd.DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	rtbd.BlendOp = D3D11_BLEND_OP_ADD;
	rtbd.SrcBlendAlpha = D3D11_BLEND_ZERO;
	rtbd.DestBlendAlpha = D3D11_BLEND_ZERO;
	rtbd.BlendOpAlpha = D3D11_BLEND_OP_ADD;
	rtbd.RenderTargetWriteMask = 0x0F;

	blendDesc.AlphaToCoverageEnable = false;
	blendDesc.RenderTarget[0] = rtbd;

	graphics->getDevice()->CreateBlendState(&blendDesc, &Transparency);
}