#include "Camera.h"
#include "D3D11Graphics.h"
#include "Input.h"

using namespace DirectX;

//--------------------------------------------------------------------------------------
// Create a camera and store a reference to graphics to create resources later
//--------------------------------------------------------------------------------------
Camera::Camera(D3D11Graphics* graphics) { this->graphics = graphics; }

//--------------------------------------------------------------------------------------
// Initialise camera including position data, spline data and buffers
//--------------------------------------------------------------------------------------
HRESULT Camera::Initialise(XMFLOAT3 position, XMFLOAT3 direction)
{
	cameraData = new CameraData();
	cameraData->Position = position;
	cameraData->Direction = direction;
	cameraData->Orientation = XMFLOAT3(XM_PI, -XM_PIDIV4, 0.0f);
	cameraData->Up = XMFLOAT3(0.0f, 1.0f, 0.0f);
	HRESULT hr = BufferFactory::CreateConstantBuffer<ViewMatrix>(graphics, &ViewBuffer);
	if (FAILED(hr))
		return hr;
	graphics->getContext()->VSSetConstantBuffers(1, 1, &ViewBuffer);
	graphics->getContext()->DSSetConstantBuffers(1, 1, &ViewBuffer);

	hr = BufferFactory::CreateConstantBuffer<CameraConstBuffer>(graphics, &CameraDataBuffer);
	if (FAILED(hr))
		return hr;

	graphics->getContext()->GSSetConstantBuffers(3, 1, &CameraDataBuffer);
	graphics->getContext()->DSSetConstantBuffers(3, 1, &CameraDataBuffer);
	graphics->getContext()->PSSetConstantBuffers(3, 1, &CameraDataBuffer);

	SetupCameraTrack(0.5f, XMFLOAT2(0.0f, 0.0f), 7.0f, 5.0f, 2.0f, 2.5f);

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Create a camera track comprised of 4 bezier splines based on variables supplied
//--------------------------------------------------------------------------------------
void Camera::SetupCameraTrack(float speed, DirectX::XMFLOAT2 xzCentre, float radius, float yCentre, float oscillation, float yLookAt)
{
	float rDiv2 = radius / 2.0f;
	float maxY = yCentre + oscillation; float miny = yCentre - oscillation;
	float xCentre = xzCentre.x; float zCentre = xzCentre.y;

	CameraPathSplines.push_back(new BezierSpline(XMFLOAT3(xCentre, yCentre, zCentre + radius), XMFLOAT3(xCentre + rDiv2, maxY, zCentre + radius),
												 XMFLOAT3(xCentre + radius, maxY, zCentre + rDiv2), XMFLOAT3(xCentre + radius, yCentre, zCentre)));
	CameraPathSplines.push_back(new BezierSpline(XMFLOAT3(xCentre + radius, yCentre, zCentre), XMFLOAT3(xCentre + radius, miny, zCentre - rDiv2),
												 XMFLOAT3(xCentre + rDiv2, miny, zCentre - radius), XMFLOAT3(xCentre, yCentre, zCentre - radius)));
	CameraPathSplines.push_back(new BezierSpline(XMFLOAT3(xCentre, yCentre, zCentre - radius), XMFLOAT3(xCentre - rDiv2, maxY, zCentre - radius),
												 XMFLOAT3(xCentre - radius, maxY, zCentre - rDiv2), XMFLOAT3(xCentre - radius, yCentre, zCentre)));
	CameraPathSplines.push_back(new BezierSpline(XMFLOAT3(xCentre - radius, yCentre, zCentre), XMFLOAT3(xCentre - radius, miny, zCentre + rDiv2),
												 XMFLOAT3(xCentre - rDiv2, miny, zCentre + radius), XMFLOAT3(xCentre, yCentre, zCentre + radius)));
	currentLerp = 0.0f;
	currentSplineIndex = 0;
	trackSpeed = speed;
	splineLookAt = XMFLOAT3(xCentre, yLookAt, zCentre);
	isOnSpline = false;
}

//--------------------------------------------------------------------------------------
// Update the camera position on the bezier spline camera track
//--------------------------------------------------------------------------------------
void Camera::UpdateOnSpline(float previousFrameDuration)
{
	if (currentLerp > 1.0f)
	{
		currentLerp = 0.0f;
		if (currentSplineIndex < CameraPathSplines.size() - 1) currentSplineIndex++;
		else currentSplineIndex = 0;
	}
	BezierSpline* currentSpline = CameraPathSplines[currentSplineIndex];
	cameraData->Position = ((pow((1 - currentLerp), 3)) * currentSpline->ControlPoint1) +
		((3 * currentLerp * pow(1 - currentLerp, 2)) * currentSpline->ControlPoint2) +
		(3 * pow(currentLerp, 2) * (1 - currentLerp) * currentSpline->ControlPoint3) +
		(pow(currentLerp, 3) * currentSpline->ControlPoint4);

	currentLerp += trackSpeed*previousFrameDuration;

	cameraData->Direction = splineLookAt;
}

//--------------------------------------------------------------------------------------
// Toggle camera on / off bezier spline camera track
//--------------------------------------------------------------------------------------
void Camera::ToggleCameraTrack()
{
	isOnSpline = !isOnSpline;
}

//--------------------------------------------------------------------------------------
// Update camera off bezier spline track based on user input
//--------------------------------------------------------------------------------------
void Camera::Update(float previousFrameDuration)
{
	if (isOnSpline)
		UpdateOnSpline(previousFrameDuration);
	else
		ApplyRotation(*cameraData);

	ViewMatrix viewMatrix;
	viewMatrix.View = XMMatrixLookAtLH(XMLoadFloat3(&cameraData->Position), XMLoadFloat3(&cameraData->Direction), XMLoadFloat3(&cameraData->Up));
	graphics->getContext()->UpdateSubresource(ViewBuffer, 0, NULL, &viewMatrix, 0, 0);

	CameraConstBuffer *camera = new CameraConstBuffer(); camera->Position = cameraData->Position;
	graphics->getContext()->UpdateSubresource(CameraDataBuffer, 0, NULL, camera, 0, 0);

}

//--------------------------------------------------------------------------------------
// Apply correct rotation to camera based on first person camera principles
//--------------------------------------------------------------------------------------
void Camera::ApplyRotation(CameraData& cameraData)
{
	if (cameraData.Orientation.x < 0.1f)
		cameraData.Orientation.x = 0.1f;
	if (cameraData.Orientation.x > XM_PI - 0.1f)
		cameraData.Orientation.x = XM_PI - 0.1f;
		
	// calculate the look at rotation y
	float tempZLoc = sin(cameraData.Orientation.x);
	float tempYLoc = cos(cameraData.Orientation.x);
	float tempXLoc = 0;

	// calculate the look at rotation x
	float newX = tempZLoc * cos(cameraData.Orientation.y);
	float newZ = tempZLoc * sin(cameraData.Orientation.y);
	tempXLoc = newX;
	tempZLoc = newZ;

	cameraData.Direction.x = cameraData.Position.x + tempXLoc;
	cameraData.Direction.y = cameraData.Position.y + tempYLoc;
	cameraData.Direction.z = cameraData.Position.z + tempZLoc;
}

//--------------------------------------------------------------------------------------
// Return camera data (position, direction, up, orientation)
//--------------------------------------------------------------------------------------
CameraData* Camera::GetCameraData()
{
	return cameraData;
}

//--------------------------------------------------------------------------------------
// Clean Up camera resources
//--------------------------------------------------------------------------------------
void Camera::CleanUp()
{
	ViewBuffer->Release();
	CameraDataBuffer->Release();
}