#include "BufferFactory.h"
#include "D3D11Graphics.h"

//--------------------------------------------------------------------------------------
// Create a vertex buffer based on the vector of vertices supplied
//--------------------------------------------------------------------------------------
HRESULT BufferFactory::CreateVertexBuffer(D3D11Graphics* graphics, std::vector<Vertex> vertices, ID3D11Buffer** vertexBuffer)
{
	// Fill a buffer description
	D3D11_BUFFER_DESC bufferDesc;
	ZeroMemory(&bufferDesc, sizeof(bufferDesc));
	bufferDesc.ByteWidth = sizeof(Vertex) * vertices.size();
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = 0;

	// Define the resource data
	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = vertices.data();

	// Create the buffer
	HRESULT hr = graphics->getDevice()->CreateBuffer(&bufferDesc, &InitData, vertexBuffer);
	if (FAILED(hr))
		return hr;
	return S_OK;
}

//--------------------------------------------------------------------------------------
// Create an index buffer based on the vector of indices supplied
//--------------------------------------------------------------------------------------
HRESULT BufferFactory::CreateIndexBuffer(D3D11Graphics* graphics, std::vector<unsigned int> indices, ID3D11Buffer** indexBuffer)
{
	// Fill in a buffer description.
	D3D11_BUFFER_DESC bufferDesc;
	ZeroMemory(&bufferDesc, sizeof(bufferDesc));
	bufferDesc.ByteWidth = sizeof(unsigned int) * indices.size();
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bufferDesc.CPUAccessFlags = 0;

	// Define the resource data.
	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = indices.data();
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;

	// Create the buffer
	HRESULT hr = graphics->getDevice()->CreateBuffer(&bufferDesc, &InitData, indexBuffer);
	if (FAILED(hr))
		return hr;

	return S_OK;
}