#ifndef DATASTRUCTURES_H
#define DATASTRUCTURES_H

#include <directxmath.h>

//--------------------------------------------------------------------------------------
// Basic vertex data structure
//--------------------------------------------------------------------------------------
struct Vertex
{
	DirectX::XMFLOAT3 Position;
	DirectX::XMFLOAT3 Normal;
	DirectX::XMFLOAT2 TexCoord;
	DirectX::XMFLOAT3 Tangent;
};

//--------------------------------------------------------------------------------------
// Vertex structure to be used by Screen Space objects
//--------------------------------------------------------------------------------------
struct ScreenSpaceVertex
{
	DirectX::XMFLOAT3 Position;
	DirectX::XMFLOAT2 TexCoord;
};

//--------------------------------------------------------------------------------------
// World matrix data structure
//--------------------------------------------------------------------------------------
struct WorldMatrix
{
	DirectX::XMMATRIX World;
};

//--------------------------------------------------------------------------------------
// View matrix data structure
//--------------------------------------------------------------------------------------
struct ViewMatrix
{
	DirectX::XMMATRIX View;
};

//--------------------------------------------------------------------------------------
// Projection matrix data structure
//--------------------------------------------------------------------------------------
struct ProjectionMatrix
{
	DirectX::XMMATRIX Projection;
};

//--------------------------------------------------------------------------------------
// Camera data structure
//--------------------------------------------------------------------------------------
struct CameraData
{
	DirectX::XMFLOAT3 Position;
	DirectX::XMFLOAT3 Direction;
	DirectX::XMFLOAT3 Orientation;
	DirectX::XMFLOAT3 Up;
};

//--------------------------------------------------------------------------------------
// Camera position data structure to be passed to shaders
//--------------------------------------------------------------------------------------
struct CameraConstBuffer
{
	DirectX::XMFLOAT3 Position;
	float pad;
};

//--------------------------------------------------------------------------------------
// Basic structure to form a bezier spline
//--------------------------------------------------------------------------------------
struct BezierSpline
{
	DirectX::XMFLOAT3 ControlPoint1;
	DirectX::XMFLOAT3 ControlPoint2;
	DirectX::XMFLOAT3 ControlPoint3;
	DirectX::XMFLOAT3 ControlPoint4;

	BezierSpline(DirectX::XMFLOAT3 ControlPoint1, DirectX::XMFLOAT3 ControlPoint2,
				 DirectX::XMFLOAT3 ControlPoint3, DirectX::XMFLOAT3 ControlPoint4)
	{
		this->ControlPoint1 = ControlPoint1;
		this->ControlPoint2 = ControlPoint2;
		this->ControlPoint3 = ControlPoint3;
		this->ControlPoint4 = ControlPoint4;
	}
};

//--------------------------------------------------------------------------------------
// Data structure containing light data for second pass of rendering
//--------------------------------------------------------------------------------------
struct LightData
{
	DirectX::XMFLOAT4 Position[16];
	DirectX::XMFLOAT4 Direction[16];
	DirectX::XMFLOAT4 Diffuse[16];
	DirectX::XMFLOAT4 Specular[16];
	DirectX::XMINT4 type[16];
	float cone[16];
	int lightCount;
	int pad[3];
};

//--------------------------------------------------------------------------------------
// Data structure containing tessellation data to be used by shader
//--------------------------------------------------------------------------------------
struct TessellationData
{
	float scale;
	float radius;
	int pad[2];
};

//--------------------------------------------------------------------------------------
// Prototyping of operators for XMFLOAT3
//--------------------------------------------------------------------------------------
DirectX::XMFLOAT3 operator*(DirectX::XMFLOAT3 l, float r);
DirectX::XMFLOAT3 operator*(float l, DirectX::XMFLOAT3 r);
DirectX::XMFLOAT3 operator+(DirectX::XMFLOAT3 l, DirectX::XMFLOAT3 r);
DirectX::XMFLOAT3 operator-(DirectX::XMFLOAT3 l, DirectX::XMFLOAT3 r);
DirectX::XMFLOAT2 operator-(DirectX::XMFLOAT2 l, DirectX::XMFLOAT2 r);

DirectX::XMFLOAT3 NormalizeXMFLOAT3(DirectX::XMFLOAT3 vector);
float Dot(DirectX::XMFLOAT3 first, DirectX::XMFLOAT3 second);
DirectX::XMFLOAT3 Cross(DirectX::XMFLOAT3 first, DirectX::XMFLOAT3 second);

#endif