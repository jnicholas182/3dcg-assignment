#include "GeometryShader.h"

//--------------------------------------------------------------------------------------
// Create a new geometry shader
//--------------------------------------------------------------------------------------
HRESULT GeometryShader::Initialise(std::wstring filename, D3D11Graphics* graphics)
{
	HRESULT hr = CreateGeometryShader(filename, graphics->getDevice());
	if (FAILED(hr))
		return hr;
	return S_OK;
}

//--------------------------------------------------------------------------------------
// Set the geometry shader as the active geometry shader
//--------------------------------------------------------------------------------------
void GeometryShader::SetActive(D3D11Graphics* graphics)
{
	graphics->getContext()->GSSetShader(geometryShader, NULL, 0);
}

//--------------------------------------------------------------------------------------
// Clean up resources
//--------------------------------------------------------------------------------------
void GeometryShader::CleanUp()
{
	if (geometryShader) geometryShader->Release();
}

//--------------------------------------------------------------------------------------
// Compile the geometry shader from a specified FX file and create the geometry shader
//--------------------------------------------------------------------------------------
HRESULT GeometryShader::CreateGeometryShader(std::wstring filename, ID3D11Device* g_pd3dDevice)
{
	// Compile the geometry shader
	ID3DBlob* pGSBlob = NULL;
	if (FAILED(CompileShaderFromFile(filename.c_str(), "GS", "gs_4_0", &pGSBlob)))
	{
		MessageBox(NULL, "The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", "Error", MB_OK);
		return E_FAIL;
	}

	// Create the geometry shader
	if (FAILED(g_pd3dDevice->CreateGeometryShader(pGSBlob->GetBufferPointer(), pGSBlob->GetBufferSize(), NULL, &geometryShader)))
	{
		pGSBlob->Release();
		return E_FAIL;
	}
	return S_OK;
}