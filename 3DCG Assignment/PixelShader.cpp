#include "PixelShader.h"

//--------------------------------------------------------------------------------------
// Create a new pixel shader
//--------------------------------------------------------------------------------------
HRESULT PixelShader::Initialise(std::wstring filename, D3D11Graphics* graphics)
{
	HRESULT hr = CreatePixelShader(filename, graphics->getDevice());
	if (FAILED(hr))
		return hr;
	return S_OK;
}

//--------------------------------------------------------------------------------------
// Set the pixel shader as the active pixel shader
//--------------------------------------------------------------------------------------
void PixelShader::SetActive(D3D11Graphics* graphics)
{
	graphics->getContext()->PSSetShader(pixelShader, NULL, 0);
}

//--------------------------------------------------------------------------------------
// Clean up resources
//--------------------------------------------------------------------------------------
void PixelShader::CleanUp()
{
	if (pixelShader) pixelShader->Release();
}

//--------------------------------------------------------------------------------------
// Compile the pixel shader from a specified FX file and create the pixel shader
//--------------------------------------------------------------------------------------
HRESULT PixelShader::CreatePixelShader(std::wstring filename, ID3D11Device* g_pd3dDevice)
{
	// Compile the pixel shader
	ID3DBlob* pPSBlob = NULL;
	if (FAILED(CompileShaderFromFile(filename.c_str(), "PS", "ps_4_0", &pPSBlob)))
	{
		MessageBox(NULL, "The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", "Error", MB_OK);
		return E_FAIL;
	}

	// Create the pixel shader
	if (FAILED(g_pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &pixelShader)))
	{
		pPSBlob->Release();
		return E_FAIL;
	}
	return S_OK;
}