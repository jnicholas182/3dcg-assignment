#include "DataStructures.h"

using namespace DirectX;

//--------------------------------------------------------------------------------------
// Operators for XMFLOAT3 for ease of development
//--------------------------------------------------------------------------------------

XMFLOAT3 operator*(XMFLOAT3 l, float r) {
	return XMFLOAT3(l.x * r, l.y * r, l.z * r);
}

XMFLOAT3 operator*(float l, XMFLOAT3 r) {
	return XMFLOAT3(r.x * l, r.y * l, r.z * l);
}

XMFLOAT3 operator+(XMFLOAT3 l, XMFLOAT3 r) {
	return XMFLOAT3(l.x + r.x, l.y + r.y, l.z + r.z);
}

XMFLOAT3 operator-(XMFLOAT3 l, XMFLOAT3 r) {
	return XMFLOAT3(l.x - r.x, l.y - r.y, l.z - r.z);
}

XMFLOAT2 operator-(XMFLOAT2 l, XMFLOAT2 r) {
	return XMFLOAT2(l.x - r.x, l.y - r.y);
}

XMFLOAT3 NormalizeXMFLOAT3(XMFLOAT3 vector)
{
	float length = sqrt(Dot(vector, vector));
	return XMFLOAT3(vector.x / length, vector.y / length, vector.z / length);
}

float Dot(XMFLOAT3 first, XMFLOAT3 second)
{
	return ((first.x * second.x) + (first.y * second.y) + (first.z * second.z));
}

XMFLOAT3 Cross(XMFLOAT3 first, XMFLOAT3 second)
{
	return XMFLOAT3(first.y*second.z - first.z*second.y,
		first.z*second.x - first.x*second.z,
		first.x*second.y - first.y*second.x);
}