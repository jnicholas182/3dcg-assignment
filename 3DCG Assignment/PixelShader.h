#ifndef PIXELSHADER_H
#define PIXELSHADER_H

#include "Shader.h"

class PixelShader : public Shader
{
public:
	virtual HRESULT Initialise(std::wstring filename, D3D11Graphics* graphics);
	virtual void SetActive(D3D11Graphics* graphics);
	virtual void CleanUp();

private:
	ID3D11PixelShader*      pixelShader = NULL;

	HRESULT CreatePixelShader(std::wstring filename, ID3D11Device* g_pd3dDevice);
};

#endif