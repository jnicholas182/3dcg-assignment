#ifndef ENVIRONMENTMAPGAMEOBJECT_H
#define ENVIRONMENTMAPGAMEOBJECT_H

#include "GameObject.h"

class EnvironmentMapGameObject : public GameObject
{
public:
	EnvironmentMapGameObject(D3D11Graphics* graphics);
	HRESULT Initialise(const std::string& FileName, DirectX::XMFLOAT3 translation, DirectX::XMFLOAT3 rotation, float scale);
	void SetShadersAndBuffers();
	void Render();
	void CleanUp();
private:
	ID3D11ShaderResourceView* cubeMap = NULL;
};

#endif