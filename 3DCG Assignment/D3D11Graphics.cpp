#include "D3D11Graphics.h"
#include "GameObject.h"
#include "RenderTarget.h"
using namespace DirectX;

//--------------------------------------------------------------------------------------
// Create Direct3D device
//--------------------------------------------------------------------------------------
HRESULT D3D11Graphics::InitDevice(HWND g_hWnd)
{
	// Get window size
	RECT rc;
	GetClientRect(g_hWnd, &rc);
	width = rc.right - rc.left;
	height = rc.bottom - rc.top;

	if (FAILED(CreateSwapChain(g_hWnd, width, height)))
		return E_FAIL;
	if (FAILED(CreateRenderTargetView()))
		return E_FAIL;
	if (FAILED(CreateDeferredRenderTargetViews()))
		return E_FAIL;

	CreateRasterizers();

	if (FAILED(CreateDepthStencilTextureResource(width, height)))
		return E_FAIL;
	if (FAILED(CreateDepthStencilState()))
		return E_FAIL;

	g_pImmediateContext->OMSetDepthStencilState(DepthStencilState, 1);

	if (FAILED(CreateDepthStencilView()))
		return E_FAIL;
	
	g_pImmediateContext->OMSetRenderTargets(1, &g_pRenderTargetView, DepthStencilView);

	SetupViewport(width, height);

	// Create World and Projection Constant Buffers
	if (FAILED(BufferFactory::CreateConstantBuffer<WorldMatrix>(this, &WorldBuffer)))
		return E_FAIL;
	if (FAILED(BufferFactory::CreateConstantBuffer<ProjectionMatrix>(this, &ProjectionBuffer)))
		return E_FAIL;

	// Create the projection matrix and set in the projection buffer
	ProjectionMatrix projectionMatrix;
	projectionMatrix.Projection = XMMatrixPerspectiveFovLH(0.25f * XM_PI, (float)width / (float)height, 0.1f, 500.0f);
	this->g_pImmediateContext->UpdateSubresource(ProjectionBuffer, 0, NULL, &projectionMatrix, 0, 0);

	this->g_pImmediateContext->VSSetConstantBuffers(2, 1, &ProjectionBuffer);
	this->g_pImmediateContext->DSSetConstantBuffers(2, 1, &ProjectionBuffer);
	this->g_pImmediateContext->VSSetConstantBuffers(0, 1, &WorldBuffer);
	this->g_pImmediateContext->DSSetConstantBuffers(0, 1, &WorldBuffer);

	// Create the Sample State
	if (FAILED(CreateSampleState()))
		return E_FAIL;

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Create deferred render target views
//--------------------------------------------------------------------------------------
HRESULT D3D11Graphics::CreateDeferredRenderTargetViews()
{
	RenderTarget* positions = new RenderTarget();
	if (FAILED(positions->Initialise(g_pd3dDevice, (float)width, (float)height)))
		return E_FAIL;
	deferredRenderTargets.push_back(positions);

	RenderTarget* normals = new RenderTarget();
	if (FAILED(normals->Initialise(g_pd3dDevice, (float)width, (float)height)))
		return E_FAIL;
	deferredRenderTargets.push_back(normals);

	RenderTarget* colours = new RenderTarget();
	if (FAILED(colours->Initialise(g_pd3dDevice, (float)width, (float)height)))
		return E_FAIL;
	deferredRenderTargets.push_back(colours);

	renderScreenSpaceRenderTargets = false;
	return S_OK;
}

//--------------------------------------------------------------------------------------
// Return deferred render targets
//--------------------------------------------------------------------------------------
std::vector<RenderTarget*> D3D11Graphics::getRenderTargets()
{
	return deferredRenderTargets;
}

//--------------------------------------------------------------------------------------
// Set the back buffer as the active render target (second pass)
//--------------------------------------------------------------------------------------
void D3D11Graphics::SetBackBufferTargetActive()
{
	ID3D11RenderTargetView* rTargets[3] = { g_pRenderTargetView, NULL, NULL };
	g_pImmediateContext->OMSetRenderTargets(3, rTargets, DepthStencilView);
}

//--------------------------------------------------------------------------------------
// Set the deferred render targets as the active render targets (first pass)
//--------------------------------------------------------------------------------------
void D3D11Graphics::SetDeferredTargetsActive()
{
	ID3D11RenderTargetView* deferredTargets[3] = { deferredRenderTargets[0]->getRenderTarget(), 
												   deferredRenderTargets[1]->getRenderTarget(), 
												   deferredRenderTargets[2]->getRenderTarget() };

	g_pImmediateContext->OMSetRenderTargets(3, deferredTargets, DepthStencilView);
}

//--------------------------------------------------------------------------------------
// Create the Swap Chain
//--------------------------------------------------------------------------------------
HRESULT D3D11Graphics::CreateSwapChain(HWND g_hWnd, UINT width, UINT height)
{
	HRESULT hr;

	UINT createDeviceFlags = 0;
#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};
	UINT numDriverTypes = ARRAYSIZE(driverTypes);

	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	};
	UINT numFeatureLevels = ARRAYSIZE(featureLevels);

	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory(&sd, sizeof(sd));
	sd.BufferCount = 1;
	sd.BufferDesc.Width = width;
	sd.BufferDesc.Height = height;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow = g_hWnd;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		g_driverType = driverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(NULL, g_driverType, NULL, createDeviceFlags, featureLevels, numFeatureLevels,
			D3D11_SDK_VERSION, &sd, &g_pSwapChain, &g_pd3dDevice, &g_featureLevel, &g_pImmediateContext);
		if (SUCCEEDED(hr))
			break;
	}
	if (FAILED(hr))
		return hr;

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Create a render target view
//--------------------------------------------------------------------------------------
HRESULT D3D11Graphics::CreateRenderTargetView()
{
	HRESULT hr;

	ID3D11Texture2D* pBackBuffer = NULL;
	hr = g_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
	if (FAILED(hr))
		return hr;

	hr = g_pd3dDevice->CreateRenderTargetView(pBackBuffer, NULL, &g_pRenderTargetView);
	pBackBuffer->Release();
	if (FAILED(hr))
		return hr;

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Create the rasterizer
//--------------------------------------------------------------------------------------
void D3D11Graphics::CreateRasterizers()
{
	ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.DepthClipEnable = true;
	rasterizerDesc.AntialiasedLineEnable = FALSE;

	ZeroMemory(&screenSpaceRasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
	screenSpaceRasterizerDesc.FillMode = D3D11_FILL_SOLID;
	screenSpaceRasterizerDesc.CullMode = D3D11_CULL_NONE;
	screenSpaceRasterizerDesc.DepthClipEnable = true;
	screenSpaceRasterizerDesc.AntialiasedLineEnable = FALSE;
}

//--------------------------------------------------------------------------------------
// Recreate rasterizer with new description (toggling wireframe / antialiasing)
//--------------------------------------------------------------------------------------
void D3D11Graphics::SetRasterizer(D3D11_RASTERIZER_DESC* description)
{
	g_pd3dDevice->CreateRasterizerState(description, &rasterizer);
	g_pImmediateContext->RSSetState(rasterizer);
}

//--------------------------------------------------------------------------------------
// Create the Depth stencil texture resource
//--------------------------------------------------------------------------------------
HRESULT D3D11Graphics::CreateDepthStencilTextureResource(UINT width, UINT height)
{
	HRESULT hr;
	// Create depth stencil texture
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(descDepth));
	descDepth.Width = width;
	descDepth.Height = height;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;

	hr = g_pd3dDevice->CreateTexture2D(&descDepth, NULL, &DepthStencil);
	if (FAILED(hr))
		return hr;

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Create the Depth stencil state
//--------------------------------------------------------------------------------------
HRESULT D3D11Graphics::CreateDepthStencilState()
{
	HRESULT hr;
	D3D11_DEPTH_STENCIL_DESC dsDesc;
	ZeroMemory(&dsDesc, sizeof(dsDesc));
	// Depth test parameters
	dsDesc.DepthEnable = true;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsDesc.DepthFunc = D3D11_COMPARISON_LESS;

	// Stencil test parameters
	dsDesc.StencilEnable = true;
	dsDesc.StencilReadMask = 0xFF;
	dsDesc.StencilWriteMask = 0xFF;

	// Stencil operations if pixel is front-facing
	dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Stencil operations if pixel is back-facing
	dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Create depth stencil state
	hr = g_pd3dDevice->CreateDepthStencilState(&dsDesc, &DepthStencilState);
	if (FAILED(hr))
		return hr;

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Create the Depth stencil view
//--------------------------------------------------------------------------------------
HRESULT D3D11Graphics::CreateDepthStencilView()
{
	HRESULT hr;
	// Create the depth stencil view
	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;

	hr = g_pd3dDevice->CreateDepthStencilView(DepthStencil, &descDSV, &DepthStencilView);
	if (FAILED(hr))
		return hr;

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Setup the Viewport
//--------------------------------------------------------------------------------------
void D3D11Graphics::SetupViewport(UINT width, UINT height)
{
	ZeroMemory(&defaultViewport, sizeof(D3D11_VIEWPORT));
	defaultViewport.Width = (FLOAT)width;
	defaultViewport.Height = (FLOAT)height;
	defaultViewport.MinDepth = 0.0f;
	defaultViewport.MaxDepth = 1.0f;
	defaultViewport.TopLeftX = 0;
	defaultViewport.TopLeftY = 0;
	g_pImmediateContext->RSSetViewports(1, &defaultViewport);
}

//--------------------------------------------------------------------------------------
// Create the sample state
//--------------------------------------------------------------------------------------
HRESULT D3D11Graphics::CreateSampleState()
{
	HRESULT hr;

	D3D11_SAMPLER_DESC samplerStateDesc;
	ZeroMemory(&samplerStateDesc, sizeof(samplerStateDesc));
	samplerStateDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerStateDesc.MaxAnisotropy = 0;
	samplerStateDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerStateDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerStateDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerStateDesc.MipLODBias = 0.0f;
	samplerStateDesc.MinLOD = 0;
	samplerStateDesc.MaxLOD = D3D11_FLOAT32_MAX;
	samplerStateDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;

	hr = g_pd3dDevice->CreateSamplerState(&samplerStateDesc, &SamplerLinear);
	if (FAILED(hr))
		return hr;

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Clear the Back Buffer
//--------------------------------------------------------------------------------------
void D3D11Graphics::ClearBackBuffer()
{
	float ClearColor[4] = { 0.9f, 0.9f, 0.9f, 1.0f };
	g_pImmediateContext->ClearRenderTargetView(g_pRenderTargetView, ClearColor);
}

//--------------------------------------------------------------------------------------
// Create the deferred render targets to be rendered to in the first pass and
// sampled in second pass
//--------------------------------------------------------------------------------------
void D3D11Graphics::ClearDeferredRenderTargets()
{
	// Clear the back buffer 
	float ClearColor[4] = { 0.9f, 0.9f, 0.9f, 1.0f };
	g_pImmediateContext->ClearRenderTargetView(deferredRenderTargets[0]->getRenderTarget(), ClearColor);
	g_pImmediateContext->ClearRenderTargetView(deferredRenderTargets[1]->getRenderTarget(), ClearColor);
	g_pImmediateContext->ClearRenderTargetView(deferredRenderTargets[2]->getRenderTarget(), ClearColor);
	
	deferredRenderTargets[0]->ClearShaderResourceView(g_pd3dDevice);
	deferredRenderTargets[1]->ClearShaderResourceView(g_pd3dDevice);
	deferredRenderTargets[2]->ClearShaderResourceView(g_pd3dDevice);
}

//--------------------------------------------------------------------------------------
// Clear the depth stencil
//--------------------------------------------------------------------------------------
void D3D11Graphics::ClearDepthStencil()
{
	g_pImmediateContext->ClearDepthStencilView(DepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
}

//--------------------------------------------------------------------------------------
// Present the Swap Chain
//--------------------------------------------------------------------------------------
void D3D11Graphics::PresentSwapChain()
{
	g_pSwapChain->Present(1, 0);
}

//--------------------------------------------------------------------------------------
// Clean up the objects created
//--------------------------------------------------------------------------------------
void D3D11Graphics::CleanUp()
{
	if (g_pd3dDevice) g_pd3dDevice->Release();
	if (g_pImmediateContext)
	{
		g_pImmediateContext->ClearState();
		g_pImmediateContext->Release();
	}
	if (g_pSwapChain) g_pSwapChain->Release();
	if (g_pRenderTargetView) g_pRenderTargetView->Release();
	for each(RenderTarget* renderTarget in deferredRenderTargets)
	{
		renderTarget->CleanUp();
	}
	if (rasterizer) rasterizer->Release();

	if (DepthStencil) DepthStencil->Release();
	if (DepthStencilState) DepthStencilState->Release();
	if (DepthStencilView) DepthStencilView->Release();
	if (SamplerLinear) SamplerLinear->Release();

	if (WorldBuffer) WorldBuffer->Release();
	if (ProjectionBuffer) ProjectionBuffer->Release();
}

//--------------------------------------------------------------------------------------
// Return graphics device
//--------------------------------------------------------------------------------------
ID3D11Device* D3D11Graphics::getDevice() const
{
	return this->g_pd3dDevice;
}

//--------------------------------------------------------------------------------------
// Return immediate context
//--------------------------------------------------------------------------------------
ID3D11DeviceContext* D3D11Graphics::getContext() const
{
	return this->g_pImmediateContext;
}

//--------------------------------------------------------------------------------------
// Return buffer for world matrix
//--------------------------------------------------------------------------------------
ID3D11Buffer* D3D11Graphics::getWorldBuffer() const
{
	return this->WorldBuffer;
}

//--------------------------------------------------------------------------------------
// Return rasterizer description
//--------------------------------------------------------------------------------------
D3D11_RASTERIZER_DESC* D3D11Graphics::getRasterizerDesc()
{
	return &this->rasterizerDesc;
}

//--------------------------------------------------------------------------------------
// Return screen space rasterizer
//--------------------------------------------------------------------------------------
D3D11_RASTERIZER_DESC* D3D11Graphics::getScreenSpaceRasterizerDesc()
{
	return &this->screenSpaceRasterizerDesc;
}

//--------------------------------------------------------------------------------------
// Set the sampler (used for sampling textures in shaders)
//--------------------------------------------------------------------------------------
void D3D11Graphics::SetSamplerLinear()
{
	g_pImmediateContext->PSSetSamplers(0, 1, &SamplerLinear);
	g_pImmediateContext->PSSetSamplers(1, 1, &SamplerLinear);
}