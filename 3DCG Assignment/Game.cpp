#include "Game.h"
#include "GameObject.h"
#include "BumpMapGameObject.h"
#include "EnvironmentMapGameObject.h"
#include "TessellatedGameObject.h"
#include "TexturedGameObject.h"
#include "ScreenSpaceObject.h"
#include "RenderTarget.h"
#include "Input.h"
#include "LightFunctions.h"
#include <Mmsystem.h>

using namespace DirectX;

//--------------------------------------------------------------------------------------
// Create Game and store reference to graphics to create resources
//--------------------------------------------------------------------------------------
Game::Game(D3D11Graphics* graphics)
{
	this->graphics = graphics;
}

//--------------------------------------------------------------------------------------
// Initialise game and resource
//--------------------------------------------------------------------------------------
HRESULT Game::Initialise()
{
	gamepad = new Gamepad(1);

	camera = new Camera(graphics);
	if (FAILED(camera->Initialise(XMFLOAT3(0.0f, 5.0f, 0.0f), XMFLOAT3(0.0f, -1.0f, 0.0f))))
		return E_FAIL;

	SetupGameObjects();
	
	SetupScreenSpaceRenderTargets();

	if (FAILED(SetupLights()))
		return E_FAIL;

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Run game in loop
//--------------------------------------------------------------------------------------
MSG Game::Run(HWND g_hWnd)
{
	MSG msg = { 0 };
	float previousFrameDuration = 0.0f;
	while (WM_QUIT != msg.message)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			// Start update timer
			unsigned int startTime = timeGetTime();

			// Clear back buffer and depth stencil
			graphics->ClearBackBuffer();
			graphics->ClearDeferredRenderTargets();
			graphics->ClearDepthStencil();

			//If focus is on game window, process input
			if (GetFocus() == g_hWnd)
			{
				// Gamepad update and input handling
				gamepad->Update();
				Input::ReceiveControllerInput(graphics, previousFrameDuration, g_hWnd, gamepad, camera, gameObjects);

				// Handle Keyboard Input
				Input::ReceiveKeyboardInput(graphics, previousFrameDuration, g_hWnd, camera, gameObjects);
			}
			// Update Camera based on input
			camera->Update(previousFrameDuration);

			//Update lights
			UpdateLights(previousFrameDuration);

			graphics->SetDeferredTargetsActive();
			graphics->SetRasterizer(graphics->getRasterizerDesc());

			// Render every game object
			for (unsigned int i = 0; i < gameObjects.size(); i++)
			{
				gameObjects[i]->SetShadersAndBuffers();
				gameObjects[i]->Update(previousFrameDuration);
				gameObjects[i]->Render();
			}

			//Render every light object
			for (unsigned int i = 0; i < lightObjects.size(); i++)
			{
				lightObjects[i]->SetShadersAndBuffers();
				lightObjects[i]->Render();
			}

			UpdateScreenSpaceRenderTargetTextures();

			graphics->SetRasterizer(graphics->getScreenSpaceRasterizerDesc());

			// Pass lights to shader
			graphics->getContext()->UpdateSubresource(LightDataBuffer, 0, NULL, lights, 0, 0);

			graphics->SetBackBufferTargetActive();

			finalScreenQuad->SetShadersAndBuffers();
			finalScreenQuad->SetShaderResources(graphics->getRenderTargets()[0]->getShaderResourceView(),
												graphics->getRenderTargets()[1]->getShaderResourceView(),
												graphics->getRenderTargets()[2]->getShaderResourceView());
			finalScreenQuad->Render();

			// Render screen space render targets if on
			if (graphics->renderScreenSpaceRenderTargets)
			{
				for (unsigned int i = 0; i < screenSpaceRenderTargets.size(); i++)
				{
					screenSpaceRenderTargets[i]->SetShadersAndBuffers();
					screenSpaceRenderTargets[i]->SetShaderResources();
					screenSpaceRenderTargets[i]->Render();
				}
			}

			// Present the swap chain
			graphics->PresentSwapChain();

			// End timer and store time taken for update
			previousFrameDuration = (timeGetTime() - startTime) / 1000.0f;
		}
	}
	return msg;
}

//--------------------------------------------------------------------------------------
// Clean up game objects
//--------------------------------------------------------------------------------------
void Game::CleanUp()
{
	camera->CleanUp();
	for (unsigned int i = 0; i < gameObjects.size(); i++)
		gameObjects[i]->CleanUp();
	for (unsigned int i = 0; i < lightObjects.size(); i++)
		lightObjects[i]->CleanUp();
}

//--------------------------------------------------------------------------------------
// Create and Load object for scene
//--------------------------------------------------------------------------------------
void Game::SetupGameObjects()
{
	HRESULT hr;

	BumpMapGameObject* cube = new BumpMapGameObject(graphics);
	hr = cube->Initialise("Cube", XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 0.0f), 2.0f);
	if (hr == S_OK)
		gameObjects.push_back(cube);

	EnvironmentMapGameObject* sphere = new EnvironmentMapGameObject(graphics);
	hr = sphere->Initialise("ReflectiveSphere", XMFLOAT3(0.0f, 7.0f, 10.0f), XMFLOAT3(0.0f, 0.0f, 0.0f), 0.5f);
	if (hr == S_OK)
		gameObjects.push_back(sphere);

	TessellatedGameObject* TesselatedSphere = new TessellatedGameObject(graphics);
	hr = TesselatedSphere->Initialise("Sphere", XMFLOAT3(0.0f, 7.0f, -10.0f), XMFLOAT3(0.0f, 0.0f, 0.0f), 0.5f, 3.365f);
	if (hr == S_OK)
		gameObjects.push_back(TesselatedSphere);

	BumpMapGameObject* floor = new BumpMapGameObject(graphics);
	hr = floor->Initialise("Floor", XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 0.0f), 0.5f);
	if (hr == S_OK)
		gameObjects.push_back(floor);
	
	EnvironmentMapGameObject* skyBox = new EnvironmentMapGameObject(graphics);
	hr = skyBox->Initialise("SkyBox", XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 0.0f), 5.0f);
	if (hr == S_OK)
		gameObjects.push_back(skyBox);
	
	finalScreenQuad = new ScreenSpaceObject(graphics);
	hr = finalScreenQuad->Initialise(XMFLOAT2(0.0f, 0.0f), 1.0f, "Shaders/DeferredShading.fx", 0.1f);
}

//--------------------------------------------------------------------------------------
// Create screen space objects to show stages of deferred shading
//--------------------------------------------------------------------------------------
void Game::SetupScreenSpaceRenderTargets()
{
	ScreenSpaceObject* normals = new ScreenSpaceObject(graphics);
	HRESULT hr = normals->Initialise(XMFLOAT2(-0.7f, 0.7f), 0.3f);
	if (hr == S_OK)
		screenSpaceRenderTargets.push_back(normals);

	ScreenSpaceObject* colours = new ScreenSpaceObject(graphics);
	hr = colours->Initialise(XMFLOAT2(-0.1f, 0.7f), 0.3f);
	if (hr == S_OK)
		screenSpaceRenderTargets.push_back(colours);
}

//--------------------------------------------------------------------------------------
// Update Screen space textures
//--------------------------------------------------------------------------------------
void Game::UpdateScreenSpaceRenderTargetTextures()
{
	std::vector<RenderTarget*> renderTargets = graphics->getRenderTargets();
	for (unsigned int i = 0; i < screenSpaceRenderTargets.size(); i++)
		screenSpaceRenderTargets[i]->SetTexture(renderTargets[i+1]->getShaderResourceView());
}

//--------------------------------------------------------------------------------------
// Create lights in scene
//--------------------------------------------------------------------------------------
HRESULT Game::SetupLights()
{
	HRESULT hr;
	hr = BufferFactory::CreateConstantBuffer<LightData>(graphics, &LightDataBuffer);
	if (FAILED(hr))
		return hr;

	lights = new LightData();
	lights->lightCount = 0;
	//Create 4 coloured spot lights around cube
	AddLight(lights, XMFLOAT4(0.0f, 5.0f,-5.0f, 1.0f), XMFLOAT4(0.0f, -1.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f), 3, 0.2f);
	AddLight(lights, XMFLOAT4(0.0f, 5.0f, 5.0f, 1.0f), XMFLOAT4(0.0f, -1.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f), 3, 0.2f);
	AddLight(lights, XMFLOAT4(5.0f, 5.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, -1.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f), 3, 0.2f);
	AddLight(lights, XMFLOAT4(-5.0f, 5.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, -1.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 0.0f, 1.0f), 3, 0.2f);
	
	//Create white directional light to demonstrate parallel light rays
	AddLight(lights, XMFLOAT4(0.0f, 7.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f), XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f), 2);

	//Create white point light to illuminate the scene
	AddLight(lights, XMFLOAT4(0.0f, 10.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, -1.0f, 0.0f, 1.0f), XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f), XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f), 1);

	//Create 4 coloured Point lights to show attenuation
	AddLight(lights, XMFLOAT4(0.0f, 0.5f, 0.0f, 1.0f), XMFLOAT4(0.0f, -1.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), 1);
	AddLight(lights, XMFLOAT4(0.0f, 0.5f, 0.0f, 1.0f), XMFLOAT4(0.0f, -1.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), 1);
	AddLight(lights, XMFLOAT4(0.0f, 0.5f, 0.0f, 1.0f), XMFLOAT4(0.0f, -1.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), 1);
	AddLight(lights, XMFLOAT4(0.0f, 0.5f, 0.0f, 1.0f), XMFLOAT4(0.0f, -1.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), 1);


	for (int i = 0; i < lights->lightCount; i++)
	{
		TexturedGameObject* light = new TexturedGameObject(graphics);
		std::string modelName;
		if (lights->type[i].x == 1)
			modelName = "Point";
		else if (lights->type[i].x == 2)
			modelName = "Arrow";
		else
			modelName = "Cone";

		hr = light->Initialise(modelName, XMFLOAT3(lights->Position[i].x, lights->Position[i].y, lights->Position[i].z), XMFLOAT3(0.0f, 0.0f, 0.0f), 0.075f);

		if (hr == S_OK)
			lightObjects.push_back(light);
	}

	graphics->getContext()->PSSetConstantBuffers(5, 1, &LightDataBuffer);

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Update light positions and orientations in scene
//--------------------------------------------------------------------------------------
void Game::UpdateLights(float previousFrameDuration)
{
	static int frame = 0;
	frame++;
	static float angle = 0.0f;
	static float directionalAngle = 0.0f;
	angle += previousFrameDuration * 0.5f;
	directionalAngle += previousFrameDuration * 0.75f;
	float radius = 3.0f;
	float directionalRadius = 15.0f;

	for (int i = 0; i < 4; i++)
	{
		float currentAngle = angle + (XM_PIDIV2 * i);
		lights->Position[i] = XMFLOAT4(radius *sin(currentAngle), 5.0f, radius *cos(currentAngle), 1.0f);
		lightObjects[i]->TranslateAsLight(XMFLOAT3(lights->Position[i].x, lights->Position[i].y, lights->Position[i].z));

		lights->Position[i + 6] = XMFLOAT4(10.0f *sin(-currentAngle), 0.5f, 10.0f *cos(currentAngle), 1.0f);
		lightObjects[i+6]->TranslateAsLight(XMFLOAT3(lights->Position[i+6].x, lights->Position[i+6].y, lights->Position[i+6].z));
	}

	XMFLOAT3 direction = NormalizeXMFLOAT3(XMFLOAT3(directionalRadius *sin(directionalAngle), 5.0f, directionalRadius *cos(directionalAngle)));
	lights->Direction[4] = XMFLOAT4(direction.x, direction.y, direction.z, 1.0f);
	lightObjects[4]->RotateAsLight(XMFLOAT3(0.0f, directionalAngle, 0.0f));

	//Strobe(&lights->cone[5], 8, frame, 0.7f);
}