#ifndef DOMAINSHADER_H
#define DOMAINSHADER_H

#include "Shader.h"

class DomainShader : public Shader
{
public:
	virtual HRESULT Initialise(std::wstring filename, D3D11Graphics* graphics);
	virtual void SetActive(D3D11Graphics* graphics);
	virtual void CleanUp();

private:
	ID3D11DomainShader*     domainShader = NULL;

	HRESULT CreateDomainShader(std::wstring filename, ID3D11Device* g_pd3dDevice);
};

#endif