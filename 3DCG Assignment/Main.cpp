#include <Windows.h>
#include "resource.h"
#include "D3D11Graphics.h"
#include "GameObject.h"
#include "Game.h"
#include "Window.h"

using namespace DirectX;

//--------------------------------------------------------------------------------------
// Entry point to the program
//--------------------------------------------------------------------------------------
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
	// Create Window and Graphics devices
	Window* window = new Window();
	D3D11Graphics* graphics = new D3D11Graphics();

	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// Initialize Window and Graphics devices
	if (FAILED(window->InitWindow(hInstance, nCmdShow)))
		return 0;

	if (FAILED(graphics->InitDevice(window->g_hWnd)))
	{
		graphics->CleanUp();
		return 0;
	}

	// Create and Initialize a Game instance
	Game* game = new Game(graphics);
	game->Initialise();

	// Run game loop
	MSG msg = game->Run(window->g_hWnd);

	// Clean up Game and Graphics resources
	game->CleanUp();
	graphics->CleanUp();

	return (int)msg.wParam;
}