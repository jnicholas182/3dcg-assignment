#include <fstream>
#include "BumpMapGameObject.h"

using namespace DirectX;

//--------------------------------------------------------------------------------------
// Call base constructor for Game Object
//--------------------------------------------------------------------------------------
BumpMapGameObject::BumpMapGameObject(D3D11Graphics* graphics) : GameObject(graphics){}

//--------------------------------------------------------------------------------------
// Set up additional resources for Bump Map object (new vertex layout, textures)
//--------------------------------------------------------------------------------------
HRESULT BumpMapGameObject::Initialise(const std::string& FileName, XMFLOAT3 translation, XMFLOAT3 rotation, float scale)
{
	GameObject::Initialise("BumpMapShader.fx", FileName, translation, rotation, scale);
	const std::string FilePath = "Models/" + FileName + "/";

	HRESULT hr;

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	vertexShader = new VertexShader();
	vertexShader->Initialise(shaderFileName.c_str(), graphics, layout, ARRAYSIZE(layout));

	std::string t = FilePath + "COLOR.png";
	if (!exists(t.c_str()))
		t = dt;
	hr = CreateWICTextureFromFile(graphics->getDevice(), graphics->getContext(), std::wstring(t.begin(), t.end()).c_str(), nullptr, &texture);
	if (FAILED(hr))
		return hr;

	std::string n = FilePath + "NRM.png";
	if (!exists(n.c_str()))
		n = dn;
	hr = CreateWICTextureFromFile(graphics->getDevice(), graphics->getContext(), std::wstring(n.begin(), n.end()).c_str(), nullptr, &normalMap);
	if (FAILED(hr))
		return hr;

	std::string s = FilePath + "SPEC.png";
	if (!exists(s.c_str()))
		s = ds;
	hr = CreateWICTextureFromFile(graphics->getDevice(), graphics->getContext(), std::wstring(s.begin(), s.end()).c_str(), nullptr, &specularMap);
	if (FAILED(hr))
		return hr;

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Set additional resources to be active (shaders, textures)
//--------------------------------------------------------------------------------------
void BumpMapGameObject::SetShadersAndBuffers()
{
	GameObject::SetShadersAndBuffers();

	graphics->getContext()->HSSetShader(NULL, NULL, 0);
	graphics->getContext()->DSSetShader(NULL, NULL, 0);

	graphics->getContext()->PSSetShaderResources(0, 1, &texture);
	graphics->getContext()->PSSetShaderResources(1, 1, &normalMap);
	graphics->getContext()->PSSetShaderResources(2, 1, &specularMap);
}

//--------------------------------------------------------------------------------------
// Set topology and sampler for object and draw
//--------------------------------------------------------------------------------------
void BumpMapGameObject::Render()
{
	graphics->getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	graphics->SetSamplerLinear();

	graphics->getContext()->DrawIndexed(indices.size(), 0, 0);
}

//--------------------------------------------------------------------------------------
// Clean up resources
//--------------------------------------------------------------------------------------
void BumpMapGameObject::CleanUp()
{
	GameObject::CleanUp();
	texture->Release();
	normalMap->Release();
	specularMap->Release();
}
