#ifndef VERTEXSHADER_H
#define VERTEXSHADER_H

#include "Shader.h"

class VertexShader : public Shader
{
public:
	HRESULT Initialise(std::wstring filename, D3D11Graphics* graphics, D3D11_INPUT_ELEMENT_DESC* layout, UINT numElements);
	virtual void SetActive(D3D11Graphics* graphics);
	virtual void CleanUp();
	
private:
	ID3D11VertexShader*     vertexShader = NULL;
	ID3D11InputLayout*      vertexLayout = NULL;
	ID3DBlob*				pVSBlob = NULL;

	HRESULT CreateVertexShader(std::wstring filename, ID3D11Device* g_pd3dDevice);
	HRESULT CreateVertexLayout(D3D11Graphics* graphics, D3D11_INPUT_ELEMENT_DESC* layout, UINT numElements);
};

#endif