#include "RenderTarget.h"

//--------------------------------------------------------------------------------------
// Initialise a new render target
//--------------------------------------------------------------------------------------
HRESULT RenderTarget::Initialise(ID3D11Device* device, float width, float height)
{
	ZeroMemory(&textureDesc, sizeof(textureDesc));

	textureDesc.Width = (UINT)width;
	textureDesc.Height = (UINT)height;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;
	if (FAILED(device->CreateTexture2D(&textureDesc, NULL, &RenderedTexture)))
		return E_FAIL;

	renderTargetViewDesc.Format = textureDesc.Format;
	renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	renderTargetViewDesc.Texture2D.MipSlice = 0;
	if (FAILED(device->CreateRenderTargetView(RenderedTexture, &renderTargetViewDesc, &RenderTargetView)))
		return E_FAIL;

	shaderResourceViewDesc.Format = textureDesc.Format;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
	shaderResourceViewDesc.Texture2D.MipLevels = 1;
	if (FAILED(device->CreateShaderResourceView(RenderedTexture, &shaderResourceViewDesc, &ShaderResourceView)))
		return E_FAIL;
	return S_OK;
}

//--------------------------------------------------------------------------------------
// Return the texture of the render target
//--------------------------------------------------------------------------------------
ID3D11Texture2D* RenderTarget::getTexture()
{
	return RenderedTexture;
}

//--------------------------------------------------------------------------------------
// Return the render target view
//--------------------------------------------------------------------------------------
ID3D11RenderTargetView* RenderTarget::getRenderTarget()
{
	return RenderTargetView;
}

//--------------------------------------------------------------------------------------
// Return the shader resource view
//--------------------------------------------------------------------------------------
ID3D11ShaderResourceView* RenderTarget::getShaderResourceView()
{
	return ShaderResourceView;
}

//--------------------------------------------------------------------------------------
// Clear the shader resource view each pass for a clean resource to render to
//--------------------------------------------------------------------------------------
void RenderTarget::ClearShaderResourceView(ID3D11Device* device)
{
	ShaderResourceView = NULL;
	device->CreateShaderResourceView(RenderedTexture, &shaderResourceViewDesc, &ShaderResourceView);
}

//--------------------------------------------------------------------------------------
// Clean up resources
//--------------------------------------------------------------------------------------
void RenderTarget::CleanUp()
{
	if (RenderedTexture) RenderedTexture->Release();
	if (RenderTargetView) RenderTargetView->Release();
	if (ShaderResourceView) ShaderResourceView->Release();
}
