#include "HullShader.h"

//--------------------------------------------------------------------------------------
// Create a new hull shader
//--------------------------------------------------------------------------------------
HRESULT HullShader::Initialise(std::wstring filename, D3D11Graphics* graphics)
{
	HRESULT hr = CreateHullShader(filename, graphics->getDevice());
	if (FAILED(hr))
		return hr;
	return S_OK;
}

//--------------------------------------------------------------------------------------
// Set the hull shader as the active hull shader
//--------------------------------------------------------------------------------------
void HullShader::SetActive(D3D11Graphics* graphics)
{
	graphics->getContext()->HSSetShader(hullShader, NULL, 0);
}

//--------------------------------------------------------------------------------------
// Clean up resources
//--------------------------------------------------------------------------------------
void HullShader::CleanUp()
{
	if (hullShader) hullShader->Release();
}

//--------------------------------------------------------------------------------------
// Compile the hull shader from a specified FX file and create the hull shader
//--------------------------------------------------------------------------------------
HRESULT HullShader::CreateHullShader(std::wstring filename, ID3D11Device* g_pd3dDevice)
{
	// Compile the hull shader
	ID3DBlob* pHSBlob = NULL;
	if (FAILED(CompileShaderFromFile(filename.c_str(), "HS", "hs_5_0", &pHSBlob)))
	{
		MessageBox(NULL, "The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", "Error", MB_OK);
		return E_FAIL;
	}

	// Create the Hull shader
	if (FAILED(g_pd3dDevice->CreateHullShader(pHSBlob->GetBufferPointer(), pHSBlob->GetBufferSize(), NULL, &hullShader)))
	{
		pHSBlob->Release();
		return E_FAIL;
	}
	return S_OK;
}