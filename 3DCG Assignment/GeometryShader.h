#ifndef GEOMETRYSHADER_H
#define GEOMETRYSHADER_H

#include "Shader.h"

class GeometryShader : public Shader
{
public:
	virtual HRESULT Initialise(std::wstring filename, D3D11Graphics* graphics);
	virtual void SetActive(D3D11Graphics* graphics);
	virtual void CleanUp();

private:
	ID3D11GeometryShader*     geometryShader = NULL;

	HRESULT CreateGeometryShader(std::wstring filename, ID3D11Device* g_pd3dDevice);
};

#endif