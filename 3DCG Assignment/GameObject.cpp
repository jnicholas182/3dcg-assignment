#include <fstream>
#include "GameObject.h"

using namespace DirectX;

//--------------------------------------------------------------------------------------
// Base Gameobject constructors stores a reference to graphics for creation of resources
//--------------------------------------------------------------------------------------
GameObject::GameObject(D3D11Graphics* graphics) { this->graphics = graphics; }

//--------------------------------------------------------------------------------------
// Base Initialize function, creates variables / buffers used by all child classes
//--------------------------------------------------------------------------------------
HRESULT GameObject::Initialise(const std::string& shaderFile, const std::string& FileName, XMFLOAT3 translation, XMFLOAT3 rotation, float scale, float radius)
{
	std::string fullShaderFilePath = "Shaders/" + shaderFile;
	geoShaderActive = true;
	shaderFileName = std::wstring(fullShaderFilePath.begin(), fullShaderFilePath.end());

	HRESULT hr;

	hr = FileLoader::LoadOBJ("Models/" + FileName + "/MODEL.obj", vertices, indices);
	if (FAILED(hr))
		return hr;

	geometryShader = new GeometryShader();
	pixelShader = new PixelShader();

	geometryShader->Initialise(shaderFileName.c_str(), graphics);
	geometryShader->SetActive(graphics);

	pixelShader->Initialise(shaderFileName.c_str(), graphics);

	hr = BufferFactory::CreateVertexBuffer(graphics, vertices, &vertexBuffer);
	if (FAILED(hr))
		return hr;

	hr = BufferFactory::CreateIndexBuffer(graphics, indices, &indexBuffer);
	if (FAILED(hr))
		return hr;

	this->translation = translation;
	this->rotation = rotation;
	this->scale = XMFLOAT3(scale, scale, scale);

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Base clean up function to destroy resources
//--------------------------------------------------------------------------------------
void GameObject::CleanUp()
{
	vertexShader->CleanUp();
	geometryShader->CleanUp();
	pixelShader->CleanUp();

	vertexBuffer->Release();
	indexBuffer->Release();
}

//--------------------------------------------------------------------------------------
// Basic update function to be overriden by subclasses
//--------------------------------------------------------------------------------------
void GameObject::Update(float previousFrameDuration) { }

//--------------------------------------------------------------------------------------
// Base function to set the world constant buffer and base shaders to be active
//--------------------------------------------------------------------------------------
void GameObject::SetShadersAndBuffers()
{
	SetWorldConstBuffer();

	vertexShader->SetActive(graphics);
	if(geoShaderActive)
		geometryShader->SetActive(graphics);
	else
		graphics->getContext()->GSSetShader(NULL, NULL, 0);

	pixelShader->SetActive(graphics);

	SetVertexBuffer();
	SetIndexBuffer();
}

//--------------------------------------------------------------------------------------
// Sets the geometry shader to be active or inactive (toggle back face culling)
//--------------------------------------------------------------------------------------
void GameObject::SetGeometryShaderActive(bool active)
{
	geoShaderActive = active;
}

//--------------------------------------------------------------------------------------
// Returns current state of geometry shader (active or inactive)
//--------------------------------------------------------------------------------------
bool GameObject::GetGeometryShaderActive()
{
	return geoShaderActive;
}

//--------------------------------------------------------------------------------------
// Sets vertex buffer for game object to be active
//--------------------------------------------------------------------------------------
void GameObject::SetVertexBuffer()
{
	// Set vertex buffer
	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	graphics->getContext()->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
}

//--------------------------------------------------------------------------------------
// Sets index buffer for game object to be active
//--------------------------------------------------------------------------------------
void GameObject::SetIndexBuffer()
{
	// Set the index buffer.
	graphics->getContext()->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);
}

//--------------------------------------------------------------------------------------
// Sets world buffer for game object to be active
//--------------------------------------------------------------------------------------
void GameObject::SetWorldConstBuffer()
{
	WorldMatrix worldMatrix;
	worldMatrix.World = XMMatrixScaling(scale.x, scale.y, scale.z) *
						XMMatrixRotationRollPitchYaw(rotation.x, rotation.y, rotation.z) *
						XMMatrixTranslation(translation.x, translation.y, translation.z);

	graphics->getContext()->UpdateSubresource(graphics->getWorldBuffer(), 0, NULL, &worldMatrix, 0, 0);
}

//--------------------------------------------------------------------------------------
// Checks if a file exists (checking for textures etc)
//--------------------------------------------------------------------------------------
bool GameObject::exists(const char *fileName)
{
	std::ifstream infile(fileName);
	return infile.good();
}