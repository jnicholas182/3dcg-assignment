#include <fstream>
#include "TexturedGameObject.h"

using namespace DirectX;

//--------------------------------------------------------------------------------------
// Call base constructor for Game Object
//--------------------------------------------------------------------------------------
TexturedGameObject::TexturedGameObject(D3D11Graphics* graphics) : GameObject(graphics){}

//--------------------------------------------------------------------------------------
// Set up additional resources for Textured object (new vertex layout, textures)
//--------------------------------------------------------------------------------------
HRESULT TexturedGameObject::Initialise(const std::string& FileName, XMFLOAT3 translation, XMFLOAT3 rotation, float scale)
{
	GameObject::Initialise("TexturedShader.fx", FileName, translation, rotation, scale);
	const std::string FilePath = "Models/" + FileName + "/";

	HRESULT hr;

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	vertexShader = new VertexShader();
	vertexShader->Initialise(shaderFileName.c_str(), graphics, layout, ARRAYSIZE(layout));

	std::string t = FilePath + "COLOR.png";
	if (!exists(t.c_str()))
		t = dt;
	hr = CreateWICTextureFromFile(graphics->getDevice(), graphics->getContext(), std::wstring(t.begin(), t.end()).c_str(), nullptr, &texture);
	if (FAILED(hr))
		return hr;

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Set additional resources to be active (shaders, textures)
//--------------------------------------------------------------------------------------
void TexturedGameObject::SetShadersAndBuffers()
{
	GameObject::SetShadersAndBuffers();

	graphics->getContext()->HSSetShader(NULL, NULL, 0);
	graphics->getContext()->DSSetShader(NULL, NULL, 0);

	graphics->getContext()->PSSetShaderResources(0, 1, &texture);
}

//--------------------------------------------------------------------------------------
// Set topology and sampler for object and draw
//--------------------------------------------------------------------------------------
void TexturedGameObject::Render()
{
	graphics->getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	graphics->SetSamplerLinear();

	graphics->getContext()->DrawIndexed(indices.size(), 0, 0);
}

//--------------------------------------------------------------------------------------
// Update position as a light object
//--------------------------------------------------------------------------------------
void TexturedGameObject::TranslateAsLight(XMFLOAT3 position)
{
	translation = position;
}

//--------------------------------------------------------------------------------------
// Update rotation as a light object
//--------------------------------------------------------------------------------------
void TexturedGameObject::RotateAsLight(XMFLOAT3 rotation)
{
	this->rotation = rotation;
}

//--------------------------------------------------------------------------------------
// Clean up resources
//--------------------------------------------------------------------------------------
void TexturedGameObject::CleanUp()
{
	GameObject::CleanUp();
	texture->Release();
}