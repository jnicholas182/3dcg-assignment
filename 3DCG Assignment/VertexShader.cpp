#include "VertexShader.h"

//--------------------------------------------------------------------------------------
// Create a new vertex shader using the specified vertex layout
//--------------------------------------------------------------------------------------
HRESULT VertexShader::Initialise(std::wstring filename, D3D11Graphics* graphics, D3D11_INPUT_ELEMENT_DESC* layout, UINT numElements)
{
	if (FAILED(CreateVertexShader(filename, graphics->getDevice())))
		return E_FAIL;
	if (FAILED(CreateVertexLayout(graphics, layout, numElements)))
		return E_FAIL;
	return S_OK;
}

//--------------------------------------------------------------------------------------
// Set the vertex shader as the active vertex shader
//--------------------------------------------------------------------------------------
void VertexShader::SetActive(D3D11Graphics* graphics)
{
	graphics->getContext()->IASetInputLayout(vertexLayout);
	graphics->getContext()->VSSetShader(vertexShader, NULL, 0);
}

//--------------------------------------------------------------------------------------
// Clean up resources
//--------------------------------------------------------------------------------------
void VertexShader::CleanUp()
{
	if (vertexLayout) vertexLayout->Release();
	if (vertexShader) vertexShader->Release();
}

//--------------------------------------------------------------------------------------
// Compile the vertex shader from a specified FX file and create the vertex shader
//--------------------------------------------------------------------------------------
HRESULT VertexShader::CreateVertexShader(std::wstring filename, ID3D11Device* g_pd3dDevice)
{
	HRESULT hr;
	// Compile the vertex shader
	hr = CompileShaderFromFile(filename.c_str(), "VS", "vs_4_0", &pVSBlob);
	if (FAILED(hr))
	{
		MessageBox(NULL,
			"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", "Error", MB_OK);
		return hr;
	}

	// Create the vertex shader
	hr = g_pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &vertexShader);
	if (FAILED(hr))
	{
		pVSBlob->Release();
		return hr;
	}
	return S_OK;
}

//--------------------------------------------------------------------------------------
// Create the vertex layout from a specified array of input element descriptions
//--------------------------------------------------------------------------------------
HRESULT VertexShader::CreateVertexLayout(D3D11Graphics* graphics, D3D11_INPUT_ELEMENT_DESC* layout, UINT numElements)
{
	if (FAILED(graphics->getDevice()->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), &vertexLayout)))
	{
		pVSBlob->Release();
		return E_FAIL;
	}
		
	return S_OK;
}