#ifndef D3D11GRAPHICS_H
#define D3D11GRAPHICS_H

#include <d3d11.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include <directxcolors.h>
#include "resource.h"
#include "BufferFactory.h"

#include <vector>

class RenderTarget;

class D3D11Graphics{
public:
	HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
	HRESULT InitDevice(HWND g_hWnd);

	HRESULT CreateSwapChain(HWND g_hWnd, UINT width, UINT height);
	HRESULT CreateRenderTargetView();

	HRESULT CreateDeferredRenderTargetViews();

	void CreateRasterizers();

	HRESULT CreateDepthStencilTextureResource(UINT width, UINT height);
	HRESULT CreateDepthStencilState();
	HRESULT CreateDepthStencilView();

	void SetupViewport(UINT width, UINT height);
	HRESULT CreateSampleState();

	void ClearBackBuffer();
	void ClearDeferredRenderTargets();
	void ClearDepthStencil();
	void PresentSwapChain();
	void CleanUp();
	ID3D11Device* getDevice() const;
	ID3D11DeviceContext* getContext() const;
	ID3D11Buffer* getWorldBuffer() const;
	D3D11_RASTERIZER_DESC* getRasterizerDesc();
	D3D11_RASTERIZER_DESC* getScreenSpaceRasterizerDesc();
	void SetRasterizer(D3D11_RASTERIZER_DESC* description);
	void SetSamplerLinear();

	void SetBackBufferTargetActive();
	void SetDeferredTargetsActive();

	std::vector<RenderTarget*> getRenderTargets();

	UINT width, height;
	bool renderScreenSpaceRenderTargets;

private:
	D3D_DRIVER_TYPE         g_driverType = D3D_DRIVER_TYPE_NULL;
	D3D_FEATURE_LEVEL       g_featureLevel = D3D_FEATURE_LEVEL_11_0;
	ID3D11Device*           g_pd3dDevice = NULL;
	ID3D11DeviceContext*    g_pImmediateContext = NULL;
	IDXGISwapChain*         g_pSwapChain = NULL;
	ID3D11RenderTargetView* g_pRenderTargetView = NULL;

	std::vector<RenderTarget*> deferredRenderTargets;

	D3D11_RASTERIZER_DESC	rasterizerDesc, screenSpaceRasterizerDesc;
	ID3D11RasterizerState*	rasterizer = NULL;

	ID3D11Texture2D*			DepthStencil = NULL;
	ID3D11DepthStencilState*	DepthStencilState = NULL;
	ID3D11DepthStencilView*		DepthStencilView = NULL;
	ID3D11SamplerState*			SamplerLinear = NULL;
	D3D11_VIEWPORT				defaultViewport;

	ID3D11Buffer*			WorldBuffer = NULL;
	ID3D11Buffer*			ProjectionBuffer = NULL;
};
#endif