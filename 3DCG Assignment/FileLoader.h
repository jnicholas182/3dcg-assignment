#ifndef FILELOADER_H
#define FILELOADER_H

#include <string>
#include <vector>
#include <Windows.h>

#include "DataStructures.h"

class FileLoader
{
public:
	static HRESULT LoadOBJ(const std::string& Filename, std::vector<Vertex>& objectVertices, std::vector<unsigned int>& indices);
	static void CalculateTangents(std::vector<Vertex>& objectVertices, std::vector<unsigned int>& indices);
	static void split(const std::string &txt, std::vector<std::string> &strs, char delimiter);
};

#endif