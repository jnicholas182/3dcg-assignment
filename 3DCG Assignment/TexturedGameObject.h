#ifndef TEXTUREDGAMEOBJECT_H
#define TEXTUREDGAMEOBJECT_H

#include "GameObject.h"

class TexturedGameObject : public GameObject
{
public:
	TexturedGameObject(D3D11Graphics* graphics);
	HRESULT Initialise(const std::string& FileName, DirectX::XMFLOAT3 translation, DirectX::XMFLOAT3 rotation, float scale);
	void SetShadersAndBuffers();
	void Render();
	void TranslateAsLight(DirectX::XMFLOAT3 position);
	void RotateAsLight(DirectX::XMFLOAT3 rotation);
	void CleanUp();

private:
	ID3D11ShaderResourceView* texture = NULL;
};

#endif