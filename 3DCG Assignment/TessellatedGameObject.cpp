#include <fstream>
#include "TessellatedGameObject.h"
#include "BufferFactory.h"
#include "D3D11Graphics.h"
#include "VertexShader.h"
#include "HullShader.h"
#include "DomainShader.h"
#include "GeometryShader.h"
#include "PixelShader.h"
#include "FileLoader.h"
#include "WICTextureLoader.h"
#include "DDSTextureLoader.h"

using namespace DirectX;

//--------------------------------------------------------------------------------------
// Call base constructor for Game Object
//--------------------------------------------------------------------------------------
TessellatedGameObject::TessellatedGameObject(D3D11Graphics* graphics) : GameObject(graphics){}

//--------------------------------------------------------------------------------------
// Set up additional resources for Tessellated object (new vertex layout, textures)
//--------------------------------------------------------------------------------------
HRESULT TessellatedGameObject::Initialise(const std::string& FileName, XMFLOAT3 translation, XMFLOAT3 rotation, float scale, float radius)
{
	GameObject::Initialise("SphereTesselator.fx", FileName, translation, rotation, scale);
	const std::string FilePath = "Models/" + FileName + "/";

	HRESULT hr;

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	vertexShader = new VertexShader();
	vertexShader->Initialise(shaderFileName.c_str(), graphics, layout, ARRAYSIZE(layout));

	hullShader = new HullShader();
	hullShader->Initialise(shaderFileName.c_str(), graphics);

	domainShader = new DomainShader();
	domainShader->Initialise(shaderFileName.c_str(), graphics);

	tessellationData = new TessellationData();
	tessellationData->radius = radius;
	tessellationData->scale = scale;

	hr = BufferFactory::CreateConstantBuffer<TessellationData>(graphics, &tessellationDataBuffer);
	if (FAILED(hr))
		return hr;

	graphics->getContext()->UpdateSubresource(tessellationDataBuffer, 0, NULL, tessellationData, 0, 0);
	graphics->getContext()->DSSetConstantBuffers(4, 1, &tessellationDataBuffer);

	std::string t = FilePath + "COLOR.png";
	if (!exists(t.c_str()))
		t = dt;
	hr = CreateWICTextureFromFile(graphics->getDevice(), graphics->getContext(), std::wstring(t.begin(), t.end()).c_str(), nullptr, &texture);
	if (FAILED(hr))
		return hr;
	return S_OK;
}

//--------------------------------------------------------------------------------------
// Set additional resources to be active (shaders, textures)
//--------------------------------------------------------------------------------------
void TessellatedGameObject::SetShadersAndBuffers()
{
	GameObject::SetShadersAndBuffers();

	hullShader->SetActive(graphics);
	domainShader->SetActive(graphics);
	
	graphics->getContext()->PSSetShaderResources(0, 1, &texture);
}

void TessellatedGameObject::Update(float previousFrameDuration)
{
	float speed = 1.0f * previousFrameDuration;
	rotation.y += speed;
}

//--------------------------------------------------------------------------------------
// Set topology and sampler for object and draw
//--------------------------------------------------------------------------------------
void TessellatedGameObject::Render()
{
	graphics->getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);
	graphics->SetSamplerLinear();

	graphics->getContext()->DrawIndexed(indices.size(), 0, 0);
}

//--------------------------------------------------------------------------------------
// Clean up resources
//--------------------------------------------------------------------------------------
void TessellatedGameObject::CleanUp()
{
	GameObject::CleanUp();
	hullShader->CleanUp();
	domainShader->CleanUp();
	texture->Release();
	tessellationDataBuffer->Release();
}