#ifndef CAMERA_H
#define CAMERA_H

#include <Windows.h>
#include <new>
#include <d3d11.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include "resource.h"
#include "DataStructures.h"
#include <vector>

class D3D11Graphics;

class Camera
{
public:
	Camera(D3D11Graphics* graphics);
	HRESULT Initialise(DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 direction);
	void SetupCameraTrack(float speed, DirectX::XMFLOAT2 xzCentre, float radius, float yCentre, float oscillation, float yLookAt);
	void UpdateOnSpline(float previousFrameDuration);
	void ToggleCameraTrack();
	void Update(float previousFrameDuration);
	void CleanUp();

	CameraData* GetCameraData();

	void * operator new (size_t size)
	{
		void* p = _aligned_malloc(size, 16);
		if (p == 0) throw std::bad_alloc();
		return p;
	}
	void operator delete(void *p)
	{
		_aligned_free(p);
	}

private:
	void ApplyRotation(CameraData& cameraData);
	D3D11Graphics* graphics;
	CameraData* cameraData;
	ID3D11Buffer* ViewBuffer = NULL;
	ID3D11Buffer* CameraDataBuffer = NULL;
	std::vector<BezierSpline*> CameraPathSplines;
	float currentLerp;
	float trackSpeed;
	unsigned int currentSplineIndex;
	bool isOnSpline;
	DirectX::XMFLOAT3 splineLookAt;
};

#endif