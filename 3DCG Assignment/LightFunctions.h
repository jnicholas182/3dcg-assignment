#ifndef LIGHTFUNCTIONS_H
#define LIGHTFUNCTIONS_H

#include <Windows.h>
#include <d3d11.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include "resource.h"
#include "DataStructures.h"

void AddLight(LightData* p_this, DirectX::XMFLOAT4 Position, DirectX::XMFLOAT4 Direction, DirectX::XMFLOAT4 Diffuse, DirectX::XMFLOAT4 Specular, int type, float cone = 0.0f);
void StrobeCone(float* light, int frequency, int frame, float originalCone);

#endif