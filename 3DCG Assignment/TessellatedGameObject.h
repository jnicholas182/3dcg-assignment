#ifndef TESSELLATEDGAMEOBJECT_H
#define TESSELLATEDGAMEOBJECT_H

#include "GameObject.h"

class TessellatedGameObject : public GameObject
{
public:
	TessellatedGameObject(D3D11Graphics* graphics);
	HRESULT Initialise(const std::string& FileName, DirectX::XMFLOAT3 translation, DirectX::XMFLOAT3 rotation, float scale, float radius);
	void SetShadersAndBuffers();
	void Update(float previousFrameDuration);
	void Render();
	void CleanUp();
private:
	TessellationData*		tessellationData;
	ID3D11Buffer*			tessellationDataBuffer = NULL;
	HullShader*				hullShader = NULL;
	DomainShader*			domainShader = NULL;

	ID3D11ShaderResourceView* texture = NULL;
};

#endif