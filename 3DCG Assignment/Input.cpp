#include "Input.h"
#include "GamePad.h"
#include "D3D11Graphics.h"
#include "GameObject.h"
#include "Camera.h"

#include <d3d11.h>
#include <d3dcompiler.h>
#include <directxmath.h>

using namespace DirectX;

//--------------------------------------------------------------------------------------
// Process keyboard input and react accordingly
//--------------------------------------------------------------------------------------
void Input::ReceiveKeyboardInput(D3D11Graphics* graphics, float previousFrameDuration, HWND g_hWnd, Camera* camera, std::vector<GameObject*> &gameObjects)
{
	CameraData* camData = camera->GetCameraData();

	float movement = 2.0f * previousFrameDuration;

	if (KEY_TOGGLED(VK_ESCAPE))
	{
		PostMessage(g_hWnd, WM_DESTROY, 0, 0);
	}
	if (KEY_DOWN(VK_UP))
	{
		camData->Orientation.x -= movement;
	}
	if (KEY_DOWN(VK_DOWN))
	{
		camData->Orientation.x += movement;
	}
	if (KEY_DOWN(VK_LEFT))
	{
		camData->Orientation.y += movement;
	}
	if (KEY_DOWN(VK_RIGHT))
	{
		camData->Orientation.y -= movement;
	}
	if (KEY_DOWN('W'))
	{
		camData->Position.z += movement * sin(camData->Orientation.y);
		camData->Position.x += movement * cos(camData->Orientation.y);
	}
	if (KEY_DOWN('S'))
	{
		camData->Position.z -= movement * sin(camData->Orientation.y);
		camData->Position.x -= movement * cos(camData->Orientation.y);
	}
	if (KEY_DOWN('A'))
	{
		camData->Position.z += movement * sin(camData->Orientation.y + XM_PIDIV2);
		camData->Position.x += movement * cos(camData->Orientation.y + XM_PIDIV2);
	}
	if (KEY_DOWN('D'))
	{
		camData->Position.z -= movement * sin(camData->Orientation.y + XM_PIDIV2);
		camData->Position.x -= movement * cos(camData->Orientation.y + XM_PIDIV2);
	}
	if (KEY_DOWN('E'))
	{
		camData->Position.y += movement;
	}
	if (KEY_DOWN('Q'))
	{
		camData->Position.y -= movement;
	}
	if (KEY_TOGGLED('1'))
	{
		if (graphics->getRasterizerDesc()->FillMode == D3D11_FILL_SOLID)
			graphics->getRasterizerDesc()->FillMode = D3D11_FILL_WIREFRAME;
		else if (graphics->getRasterizerDesc()->FillMode == D3D11_FILL_WIREFRAME)
			graphics->getRasterizerDesc()->FillMode = D3D11_FILL_SOLID;

		graphics->SetRasterizer(graphics->getRasterizerDesc());

	}
	if (KEY_TOGGLED('2'))
	{
		if (graphics->getRasterizerDesc()->AntialiasedLineEnable == TRUE)
			graphics->getRasterizerDesc()->AntialiasedLineEnable = FALSE;
		else if (graphics->getRasterizerDesc()->AntialiasedLineEnable == FALSE)
			graphics->getRasterizerDesc()->AntialiasedLineEnable = TRUE;
		
		graphics->SetRasterizer(graphics->getRasterizerDesc());
	}
	if (KEY_TOGGLED('3'))
	{
		for each(GameObject* gameObject in gameObjects)
		{
			if (gameObject->GetGeometryShaderActive())
				gameObject->SetGeometryShaderActive(false);
			else
				gameObject->SetGeometryShaderActive(true);
		}
	}
	if (KEY_TOGGLED('T'))
	{
		camera->ToggleCameraTrack();
	}

	if (KEY_TOGGLED('R'))
	{
		graphics->renderScreenSpaceRenderTargets = !graphics->renderScreenSpaceRenderTargets;
	}
}

//--------------------------------------------------------------------------------------
// Process gamepad input and react accordingly
//--------------------------------------------------------------------------------------
void Input::ReceiveControllerInput(D3D11Graphics* graphics, float previousFrameDuration, HWND g_hWnd, Gamepad* gamepad, Camera* camera, std::vector<GameObject*> &gameObjects)
{
	CameraData* camData = camera->GetCameraData();

	float movement = 2.0f * previousFrameDuration;
	if (!gamepad->RStick_InDeadzone())
	{
		camData->Orientation.y -= (movement*gamepad->RightStick_X());
		camData->Orientation.x -= (movement*gamepad->RightStick_Y());
	}
	if (!gamepad->LStick_InDeadzone())
	{
		camData->Position.z += ((movement*gamepad->LeftStick_Y()) * sin(camData->Orientation.y));
		camData->Position.x += ((movement*gamepad->LeftStick_Y()) * cos(camData->Orientation.y));
																						  
		camData->Position.z -= ((movement*gamepad->LeftStick_X()) * sin(camData->Orientation.y + XM_PIDIV2));
		camData->Position.x -= ((movement*gamepad->LeftStick_X()) * cos(camData->Orientation.y + XM_PIDIV2));
	}

	if (gamepad->GetButtonPressed(XButtons.L_Shoulder))
		camData->Position.y -= movement;

	if (gamepad->GetButtonPressed(XButtons.R_Shoulder))
		camData->Position.y += movement;

	if (gamepad->GetButtonReleased(XButtons.A))
	{
		camera->ToggleCameraTrack();
	}

	if (gamepad->GetButtonReleased(XButtons.B))
	{
		if (graphics->getRasterizerDesc()->FillMode == D3D11_FILL_SOLID)
			graphics->getRasterizerDesc()->FillMode = D3D11_FILL_WIREFRAME;
		else if (graphics->getRasterizerDesc()->FillMode == D3D11_FILL_WIREFRAME)
			graphics->getRasterizerDesc()->FillMode = D3D11_FILL_SOLID;

		graphics->SetRasterizer(graphics->getRasterizerDesc());
	}

	if (gamepad->GetButtonReleased(XButtons.Y))
	{
		if (graphics->getRasterizerDesc()->AntialiasedLineEnable == TRUE)
			graphics->getRasterizerDesc()->AntialiasedLineEnable = FALSE;
		else if (graphics->getRasterizerDesc()->AntialiasedLineEnable == FALSE)
			graphics->getRasterizerDesc()->AntialiasedLineEnable = TRUE;

		graphics->SetRasterizer(graphics->getRasterizerDesc());
	}

	if (gamepad->GetButtonReleased(XButtons.X))
	{
		for each(GameObject* gameObject in gameObjects)
		{
			if (gameObject->GetGeometryShaderActive())
				gameObject->SetGeometryShaderActive(false);
			else
				gameObject->SetGeometryShaderActive(true);
		}
	}

	if (gamepad->GetButtonReleased(XButtons.DPad_Down))
	{
		graphics->renderScreenSpaceRenderTargets = !graphics->renderScreenSpaceRenderTargets;
	}

	if (gamepad->GetButtonPressed(XButtons.Back))
	{
		PostMessage(g_hWnd, WM_DESTROY, 0, 0);
	}
}