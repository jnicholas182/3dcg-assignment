#include "LightFunctions.h"

using namespace DirectX;

//--------------------------------------------------------------------------------------
// Add light data to light data structure to be passed to shaders
//--------------------------------------------------------------------------------------
void AddLight(LightData* p_this, DirectX::XMFLOAT4 Position, DirectX::XMFLOAT4 Direction, DirectX::XMFLOAT4 Diffuse, DirectX::XMFLOAT4 Specular, int type, float cone)
{
	p_this->Position[p_this->lightCount] = Position;
	p_this->Direction[p_this->lightCount] = Direction;
	p_this->Diffuse[p_this->lightCount] = Diffuse;
	p_this->Specular[p_this->lightCount] = Specular;
	p_this->type[p_this->lightCount].x = type; //Type: 0 = point, 1 = directional, 2 = spotlight
	p_this->cone[p_this->lightCount] = cone;
	p_this->lightCount++;
}

//--------------------------------------------------------------------------------------
// Stobe cone lights by rapidly changing the size of the cone
//--------------------------------------------------------------------------------------
void StrobeCone(float* light, int frequency, int frame, float originalCone)
{
	*light = (frame % frequency) ? 0.0f : originalCone;
}