#ifndef BUFFERFACTORY_H
#define BUFFERFACTORY_H

#include <vector>
#include <d3d11.h>
#include <d3dcompiler.h>
#include "DataStructures.h"

class D3D11Graphics;

class BufferFactory
{
public:
	static HRESULT CreateVertexBuffer(D3D11Graphics* graphics, std::vector<Vertex> vertices, ID3D11Buffer** vertexBuffer);
	static HRESULT CreateIndexBuffer(D3D11Graphics* graphics, std::vector<unsigned int> indices, ID3D11Buffer** indexBuffer);
	template<typename T> static HRESULT CreateConstantBuffer(D3D11Graphics* graphics, ID3D11Buffer** constantBuffer)
	{
		// Fill in a buffer description.
		D3D11_BUFFER_DESC bufferDesc;
		ZeroMemory(&bufferDesc, sizeof(bufferDesc));
		bufferDesc.ByteWidth = sizeof(T);
		bufferDesc.Usage = D3D11_USAGE_DEFAULT;
		bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bufferDesc.CPUAccessFlags = 0;

		// Create the buffer
		HRESULT hr = graphics->getDevice()->CreateBuffer(&bufferDesc, NULL, constantBuffer);
		if (FAILED(hr))
			return hr;

		return S_OK;
	}
};

#endif