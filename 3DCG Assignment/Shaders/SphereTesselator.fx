//--------------------------------------------------------------------------------------
// Shader to be used by objects requiring tessellation
//--------------------------------------------------------------------------------------

#include "ShaderResource.hlsli"

//--------------------------------------------------------------------------------------
// Constant hull shader output data structure
//--------------------------------------------------------------------------------------
struct HS_CONSTANT_DATA_OUTPUT
{
	float EdgeTessFactor[3] : SV_TessFactor;
	float InsideTessFactor : SV_InsideTessFactor;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_In VS(float4 position : POSITION, float4 normal : NORMAL, float2 texCoord : TEXCOORD)
{
	PS_In output;

	output.position = mul(Projection, mul(View, mul(World, position)));
	output.normal = normalize(mul(World, float4(normal.xyz, 0.0f)));
	output.world = mul(World, position);
	output.texCoord = texCoord;

	return output;
}

//--------------------------------------------------------------------------------------
// Constant Hull Shader
//--------------------------------------------------------------------------------------
HS_CONSTANT_DATA_OUTPUT HSConstant(InputPatch<PS_In, 3> ip, uint PatchID : SV_PrimitiveID)
{
	float3 vectorToCamera = ip[0].position.xyz - CameraPosition.xyz;
	float magnitude = sqrt((vectorToCamera.x*vectorToCamera.x) + (vectorToCamera.y*vectorToCamera.y) + (vectorToCamera.z*vectorToCamera.z));

	int factor = 5 / (magnitude / 4);
	if (factor < 1) factor = 1;
	if (factor > 6) factor = 6;

	HS_CONSTANT_DATA_OUTPUT Output;
	Output.EdgeTessFactor[0] = factor;
	Output.EdgeTessFactor[1] = factor;
	Output.EdgeTessFactor[2] = factor;
	Output.InsideTessFactor = factor;

	return Output;
}


//--------------------------------------------------------------------------------------
// Control Point Hull Shader
//--------------------------------------------------------------------------------------
[domain("tri")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(3)]
[patchconstantfunc("HSConstant")]
PS_In HS(InputPatch<PS_In, 3> ip, uint i : SV_OutputControlPointID, uint PatchID : SV_PrimitiveID)
{
	PS_In Output;

	Output.position = ip[i].position;
	Output.normal = ip[i].normal;
	Output.world = ip[i].world;
	Output.texCoord = ip[i].texCoord;

	return Output;
}


//--------------------------------------------------------------------------------------
// Domain Shader
//--------------------------------------------------------------------------------------
[domain("tri")]
PS_In DS(HS_CONSTANT_DATA_OUTPUT input, float3 UV : SV_DomainLocation, const OutputPatch<PS_In, 3> patch)
{
	PS_In Output;

	Output.normal = patch[0].normal * UV.x + patch[1].normal * UV.y + patch[2].normal * UV.z;
	Output.world = patch[0].world * UV.x + patch[1].world * UV.y + patch[2].world * UV.z;
	Output.world += (normalize(Output.normal) - Output.normal) * radius * 0.5;
	Output.texCoord = patch[0].texCoord * UV.x + patch[1].texCoord * UV.y + patch[2].texCoord * UV.z;

	Output.position = mul(Projection, mul(View, Output.world));

	return Output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
PS_OUT PS(PS_In input) : SV_TARGET
{
	PS_OUT output;
	output.position = float4(input.world.xyz, 1.0f);
	output.normal = float4(input.normal.xyz, 1.0f);
	output.color = float4(textureColor.Sample(samLinear, input.texCoord).rgb, 1.0f);

	return output;
}