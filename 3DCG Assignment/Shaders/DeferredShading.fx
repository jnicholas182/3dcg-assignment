//--------------------------------------------------------------------------------------
// Shader to be used in the deferred lighting second pass
//--------------------------------------------------------------------------------------

#include "ShaderResource.hlsli"

//--------------------------------------------------------------------------------------
// Data structure written by vertex shader and read by pixel shader
//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(float4 inPos : POSITION, float2 inTexCoord : TEXCOORD)
{
	VS_OUTPUT output;

	output.Pos = inPos;
	output.TexCoord = inTexCoord;

	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_TARGET
{
	float3 position = texturePos.Sample(samLinear, input.TexCoord.xy).rgb;
	
	//Don't apply lighting to skybox
	if (position.x == 0 && position.y == 0 && position.z == 0)
		return float4(textureCol.Sample(samLinear, input.TexCoord.xy).rgb, 1.0f);

	float3 surfaceNormal = textureNorm.Sample(samLinear, input.TexCoord.xy).rgb;

	float4 finalColor;
	float3 view = normalize(CameraPosition.xyz - position.xyz);
	float3 Ambient = float3(0.0f, 0.0f, 0.0f);

	for (int i = 0; i<lightCount; i++)
	{
		float3 lightDirection = normalize(position.xyz - Position[i].xyz);

		float diffuse = max(dot(-lightDirection, surfaceNormal), 0);
		float3 reflection = normalize(reflect(lightDirection, surfaceNormal).xyz);
		float specular = pow(max(dot(view, reflection), 0), 50);

		finalColor.rgb += Ambient;

		if (type[i].x == 1)
		{
			float3 pointColor = float3(0.0f, 0.0f, 0.0f);
			pointColor.rgb += diffuse * Diffuse[i].rgb;
			pointColor.rgb += specular * Specular[i].rgb;
			float mag = length(Position[i].xyz - position.xyz);
			pointColor /= 0.0f + (1.0f * mag) + (0.0f * (mag*mag));
			finalColor.rgb += pointColor;
		}
		else if (type[i].x == 2)
		{
			finalColor.rgb += saturate(dot(Direction[i].xyz, surfaceNormal) *  Diffuse[i].rgb);
			finalColor.rgb += saturate(dot(Direction[i].xyz, surfaceNormal) * Specular[i].rgb);
		}
		else if (type[i].x == 3)
		{
			float angledot = dot(lightDirection, normalize(Direction[i].xyz));
			float cone_range = cone[i / 4][i % 4];
			float cone_threshold = 1.0 - cone_range;
			angledot = max((angledot - cone_threshold) / cone_range, 0);

			finalColor.rgb += angledot * diffuse * Diffuse[i].rgb;
			finalColor.rgb += angledot * specular * Specular[i].rgb;
		}
	}
	finalColor.rgb *= textureCol.Sample(samLinear, input.TexCoord.xy).rgb;
	return float4(finalColor.rgb, 1.0f);
}