//--------------------------------------------------------------------------------------
// Texture registers
//--------------------------------------------------------------------------------------
Texture2D textureColor : register(t0);
Texture2D textureNormal : register(t1);
Texture2D textureSpecular : register(t2);
TextureCube textureCubeMap : register(t4);
Texture2D screenTex : register(t5);
Texture2D texturePos : register(t6);
Texture2D textureNorm : register(t7);
Texture2D textureCol : register(t8);

//--------------------------------------------------------------------------------------
// Texture sampler
//--------------------------------------------------------------------------------------
SamplerState samLinear : register(s0);

//--------------------------------------------------------------------------------------
// Cube map sampler
//--------------------------------------------------------------------------------------
SamplerState CubeMapSampler
{
	Filter = ANISOTROPIC;
	MaxAnisotropy = 4;

	AddressU = WRAP;
	AddressV = WRAP;
};

//--------------------------------------------------------------------------------------
// World / View / Projection buffers
//--------------------------------------------------------------------------------------
cbuffer WorldConstantBuffer : register(b0)
{
	matrix World;
}
cbuffer ViewConstantBuffer : register(b1)
{
	matrix View;
}
cbuffer ProjectionConstantBuffer : register(b2)
{
	matrix Projection;
}

//--------------------------------------------------------------------------------------
// Camera position buffer
//--------------------------------------------------------------------------------------
cbuffer CameraData : register(b3)
{
	float4 CameraPosition;
}

//--------------------------------------------------------------------------------------
// Tessellation data buffer
//--------------------------------------------------------------------------------------
cbuffer TessellationData : register(b4)
{
	float scale;
	float radius;
}

//--------------------------------------------------------------------------------------
// Light data buffer
//--------------------------------------------------------------------------------------
cbuffer LightData : register(b5)
{
	float4 Position[16];
	float4 Direction[16];
	float4 Diffuse[16];
	float4 Specular[16];
	int4 type[16];
	float4 cone[4];
	int lightCount;
};

//--------------------------------------------------------------------------------------
// Data structure written by vertex shader and read by pixel shader
//--------------------------------------------------------------------------------------
struct PS_In
{
	float4 position : SV_POSITION;
	float4 world : POSITION;
	float4 normal : NORMAL;
	float2 texCoord : TEXCOORD;
	float4 tangent : TANGENT;
};

//--------------------------------------------------------------------------------------
// Data structure written by pixel shaders in first pass
//--------------------------------------------------------------------------------------
struct PS_OUT
{
	float4 position : COLOR0;
	float4 normal : COLOR1;
	float4 color : COLOR2;
};

//--------------------------------------------------------------------------------------
// Geometry Shader
//--------------------------------------------------------------------------------------
[maxvertexcount(3)]
void GS(triangle PS_In input[3], inout TriangleStream< PS_In > output)
{
	// Back face Culling
	float3 edge1 = input[1].position.xyz - input[0].position.xyz;
	float3 edge2 = input[2].position.xyz - input[0].position.xyz;
	float3 faceNormal = cross(edge1, edge2);

	float4 camPos = mul(Projection, mul(View, mul(World, CameraPosition)));
	float3 vectorToCamera = camPos.xyz - input[0].position.xyz;

	for (int i = 0; i < 3; i++)
	{
	if (dot(faceNormal, vectorToCamera) <= 0.0f)
		return;
	else
	{
		for (int j = 0; j < 3; j++)
		{

			PS_In element = input[j];
			output.Append(element);
		}
	}

	}
}

//--------------------------------------------------------------------------------------
// Calculate new pixel surface normal based on normal map data
//--------------------------------------------------------------------------------------
float3 CalcBumpNormal(PS_In input)
{
	float3 Normal = normalize(input.normal.xyz);
	float3 Tangent = normalize(input.tangent.xyz);

	float3 Bitangent = normalize(cross(Tangent, Normal));
	float3 BumpMapNormal = textureNormal.Sample(samLinear, input.texCoord).xyz;
	BumpMapNormal = 2.0 * BumpMapNormal - float3(1.0, 1.0, 1.0);

	float3 NewNormal;
	float3x3 TBN = float3x3(Tangent, Bitangent, Normal);

	NewNormal = mul(BumpMapNormal, TBN);
	NewNormal = normalize(NewNormal);
	return NewNormal;
}