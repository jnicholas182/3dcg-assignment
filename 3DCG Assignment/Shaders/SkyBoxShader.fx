//--------------------------------------------------------------------------------------
// Shader to be used to render a skybox
//--------------------------------------------------------------------------------------

#include "ShaderResource.hlsli"

//--------------------------------------------------------------------------------------
// Data structure written by vertex shader and read by pixel shader
//--------------------------------------------------------------------------------------
struct Skybox_PS_In
{
	float4 position : SV_POSITION;
	float4 normal : NORMAL;
	float3 texCoord : TEXCOORD;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
Skybox_PS_In VS(float4 position : POSITION, float4 normal : NORMAL, float2 texCoord : TEXCOORD)
{
	Skybox_PS_In output;

	output.position = mul(Projection, mul(View, mul(World, position))).xyzw;
	output.normal = normal;
	output.texCoord = position.xyz;

	return output;
}

//--------------------------------------------------------------------------------------
// Geometry Shader
//--------------------------------------------------------------------------------------
[maxvertexcount(3)]
void GS(triangle Skybox_PS_In input[3], inout TriangleStream< Skybox_PS_In > output)
{
	// Back face Culling
	float3 edge1 = input[1].position.xyz - input[0].position.xyz;
	float3 edge2 = input[2].position.xyz - input[0].position.xyz;
	float3 faceNormal = cross(edge1, edge2);

	float4 camPos = mul(Projection, mul(View, mul(World, CameraPosition)));
	float3 vectorToCamera = camPos.xyz - input[0].position.xyz;

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{

			Skybox_PS_In element = input[j];
			output.Append(element);
		}
	}
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
PS_OUT PS(Skybox_PS_In input) : SV_TARGET
{
	float3 surfaceNormal = input.normal.xyz;

	PS_OUT output;
	output.position = float4(0.0f, 0.0f, 0.0f, 1.0f);
	output.normal = float4(surfaceNormal.xyz, 1.0f);
	output.color = float4(textureCubeMap.Sample(CubeMapSampler, input.texCoord).rgb, 1.0f);

	return output;
}