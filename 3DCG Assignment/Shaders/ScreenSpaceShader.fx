//--------------------------------------------------------------------------------------
// Shader to be used by objects being drawn to screen space
//--------------------------------------------------------------------------------------

#include "ShaderResource.hlsli"

//--------------------------------------------------------------------------------------
// Data structure written by vertex shader and written by pixel shader
//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(float4 inPos : POSITION, float2 inTexCoord : TEXCOORD)
{
	VS_OUTPUT output;

	output.Pos = inPos;
	output.TexCoord = inTexCoord;

	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) :SV_TARGET
{
	return float4(screenTex.Sample(samLinear, input.TexCoord).rgb, 1.0f);
}