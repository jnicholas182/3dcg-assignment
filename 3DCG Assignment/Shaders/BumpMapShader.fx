//--------------------------------------------------------------------------------------
// Shader to be used by objects requiring bump mapping
//--------------------------------------------------------------------------------------

#include "ShaderResource.hlsli"

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_In VS(float4 position : POSITION, float4 normal : NORMAL, float2 texCoord : TEXCOORD, float4 tangent : TANGENT)
{
	PS_In output;
	
	output.position = mul(Projection, mul(View, mul(World, position)));
	output.normal   = normalize(mul(World, float4(normal.xyz, 0.0f)));
	output.world	= mul(World, position);
	output.texCoord = texCoord;
	output.tangent  = normalize(mul(World, tangent));

	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
PS_OUT PS(PS_In input) : SV_TARGET
{
	float3 surfaceNormal = CalcBumpNormal(input);

	PS_OUT output;
	output.position = float4(input.world.xyz, 1.0f);
	output.normal = float4(surfaceNormal.xyz, 1.0f);
	output.color = float4(textureColor.Sample(samLinear, input.texCoord).rgb, 1.0f);

	return output;
}