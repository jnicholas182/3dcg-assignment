#ifndef BUMPMAPGAMEOBJECT_H
#define BUMPMAPGAMEOBJECT_H

#include "GameObject.h"

class BumpMapGameObject : public GameObject
{
public:
	BumpMapGameObject(D3D11Graphics* graphics);
	HRESULT Initialise(const std::string& FileName, DirectX::XMFLOAT3 translation, DirectX::XMFLOAT3 rotation, float scale);
	void SetShadersAndBuffers();
	void Render();
	void CleanUp();
private:
	ID3D11ShaderResourceView* texture = NULL;
	ID3D11ShaderResourceView* normalMap = NULL;
	ID3D11ShaderResourceView* specularMap = NULL;
};

#endif