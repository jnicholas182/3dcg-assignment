#ifndef INPUT_H
#define INPUT_H

#define KEY_DOWN(virt_key_code)  ( (GetAsyncKeyState(virt_key_code) & 0x8000) ? 1 : 0 )
#define KEY_TOGGLED(virt_key_code)  ( (GetAsyncKeyState(virt_key_code) & 1) ? 1 : 0 )

#include <Windows.h>
#include <vector>
#include "DataStructures.h"

class Gamepad;
class D3D11Graphics;
class GameObject;
class Camera;

class Input
{
public:
	static void ReceiveKeyboardInput(D3D11Graphics* graphics, float previousFrameDuration, HWND g_hWnd, Camera* camera, std::vector<GameObject*> &gameObjects);
	static void ReceiveControllerInput(D3D11Graphics* graphics, float previousFrameDuration, HWND g_hWnd, Gamepad* gamepad, Camera* camera, std::vector<GameObject*> &gameObjects);
};

#endif