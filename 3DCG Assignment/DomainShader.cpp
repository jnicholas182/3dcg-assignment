#include "DomainShader.h"

//--------------------------------------------------------------------------------------
// Create a new domain shader
//--------------------------------------------------------------------------------------
HRESULT DomainShader::Initialise(std::wstring filename, D3D11Graphics* graphics)
{
	HRESULT hr = CreateDomainShader(filename, graphics->getDevice());
	if (FAILED(hr))
		return hr;
	return S_OK;
}

//--------------------------------------------------------------------------------------
// Set the domain shader as the active domain shader
//--------------------------------------------------------------------------------------
void DomainShader::SetActive(D3D11Graphics* graphics)
{
	graphics->getContext()->DSSetShader(domainShader, NULL, 0);
}

//--------------------------------------------------------------------------------------
// Clean up resources
//--------------------------------------------------------------------------------------
void DomainShader::CleanUp()
{
	if (domainShader) domainShader->Release();
}

//--------------------------------------------------------------------------------------
// Compile the domain shader from a specified FX file and create the domain shader
//--------------------------------------------------------------------------------------
HRESULT DomainShader::CreateDomainShader(std::wstring filename, ID3D11Device* g_pd3dDevice)
{
	// Compile the Domain shader
	ID3DBlob* pDSBlob = NULL;
	if (FAILED(CompileShaderFromFile(filename.c_str(), "DS", "ds_5_0", &pDSBlob)))
	{
		MessageBox(NULL, "The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", "Error", MB_OK);
		return E_FAIL;
	}

	// Create the Domain shader
	if (FAILED(g_pd3dDevice->CreateDomainShader(pDSBlob->GetBufferPointer(), pDSBlob->GetBufferSize(), NULL, &domainShader)))
	{
		pDSBlob->Release();
		return E_FAIL;
	}
	return S_OK;
}