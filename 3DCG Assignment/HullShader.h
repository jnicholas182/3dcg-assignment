#ifndef HULLSHADER_H
#define HULLSHADER_H

#include "Shader.h"

class HullShader : public Shader
{
public:
	virtual HRESULT Initialise(std::wstring filename, D3D11Graphics* graphics);
	virtual void SetActive(D3D11Graphics* graphics);
	virtual void CleanUp();

private:
	ID3D11HullShader*     hullShader = NULL;

	HRESULT CreateHullShader(std::wstring filename, ID3D11Device* g_pd3dDevice);
};

#endif