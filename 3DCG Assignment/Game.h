#ifndef GAME_H
#define GAME_H

#include "D3D11Graphics.h"
#include "Camera.h"
#include "GamePad.h"

class Light;
class GameObject;
class TexturedGameObject;
class ScreenSpaceObject;

class Game
{
public:
	Game(D3D11Graphics* graphics);
	~Game();
	HRESULT Initialise();
	MSG Run(HWND g_hWnd);
	void CleanUp();
	void SetupGameObjects();
	void SetupScreenSpaceRenderTargets();
	void UpdateScreenSpaceRenderTargetTextures();
	HRESULT SetupLights();
	void UpdateLights(float previousFrameDuration);

private:
	std::vector<GameObject*> gameObjects;
	std::vector<TexturedGameObject*> lightObjects;
	std::vector<ScreenSpaceObject*> screenSpaceRenderTargets;
	ScreenSpaceObject* finalScreenQuad;
	LightData* lights;
	ID3D11Buffer* LightDataBuffer = NULL;
	ID3D11Buffer* tessellationDataBuffer = NULL;

	D3D11Graphics* graphics;
	Camera* camera;
	Gamepad* gamepad;
};

#endif
