#include <fstream>
#include "EnvironmentMapGameObject.h"

using namespace DirectX;

//--------------------------------------------------------------------------------------
// Call base constructor for Game Object
//--------------------------------------------------------------------------------------
EnvironmentMapGameObject::EnvironmentMapGameObject(D3D11Graphics* graphics) : GameObject(graphics){}

//--------------------------------------------------------------------------------------
// Set up additional resources for Environment Map object (new vertex layout, textures)
//--------------------------------------------------------------------------------------
HRESULT EnvironmentMapGameObject::Initialise(const std::string& FileName, XMFLOAT3 translation, XMFLOAT3 rotation, float scale)
{
	if (FileName == "SkyBox")
		GameObject::Initialise("SkyBoxShader.fx", FileName, translation, rotation, scale);
	else
		GameObject::Initialise("CubeMapShader.fx", FileName, translation, rotation, scale);
	const std::string FilePath = "Models/" + FileName + "/";

	HRESULT hr;

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	vertexShader = new VertexShader();
	vertexShader->Initialise(shaderFileName.c_str(), graphics, layout, ARRAYSIZE(layout));

	std::string cm = FilePath + "SkyBox.dds";

	if (exists(cm.c_str()))
		hr = CreateDDSTextureFromFile(graphics->getDevice(), std::wstring(cm.begin(), cm.end()).c_str(), nullptr, &cubeMap);
	else
		return E_FAIL;

	if (FAILED(hr))
		return hr;

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Set additional resources to be active (shaders, textures)
//--------------------------------------------------------------------------------------
void EnvironmentMapGameObject::SetShadersAndBuffers()
{
	GameObject::SetShadersAndBuffers();

	graphics->getContext()->HSSetShader(NULL, NULL, 0);
	graphics->getContext()->DSSetShader(NULL, NULL, 0);

	graphics->getContext()->PSSetShaderResources(4, 1, &cubeMap);
}

//--------------------------------------------------------------------------------------
// Set topology for object and draw
//--------------------------------------------------------------------------------------
void EnvironmentMapGameObject::Render()
{
	graphics->getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	graphics->getContext()->DrawIndexed(indices.size(), 0, 0);
}

//--------------------------------------------------------------------------------------
// Clean up resources
//--------------------------------------------------------------------------------------
void EnvironmentMapGameObject::CleanUp()
{
	GameObject::CleanUp();
	cubeMap->Release();
}