#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <vector>
#include <d3d11.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include <directxcolors.h>
#include "resource.h"
#include "DataStructures.h"

#include "BufferFactory.h"
#include "D3D11Graphics.h"
#include "VertexShader.h"
#include "HullShader.h"
#include "DomainShader.h"
#include "GeometryShader.h"
#include "PixelShader.h"
#include "FileLoader.h"
#include "WICTextureLoader.h"
#include "DDSTextureLoader.h"

class GameObject
{
public:
	GameObject(D3D11Graphics* graphics);
	virtual HRESULT Initialise(const std::string& shaderFile, const std::string& FileName, DirectX::XMFLOAT3 translation, DirectX::XMFLOAT3 rotation, float scale, float radius = 0.0f);

	virtual void CleanUp();
	virtual void SetShadersAndBuffers();
	virtual void Render(){}
	virtual void Update(float previousFrameDuration);
	void SetVertexBuffer();
	void SetIndexBuffer();
	void SetWorldConstBuffer();
	void SetGeometryShaderActive(bool active);
	bool GetGeometryShaderActive();
	bool exists(const char *fileName);

protected:
	D3D11Graphics*			graphics;

	bool geoShaderActive;
	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;
	std::wstring shaderFileName;

	DirectX::XMFLOAT3		translation, rotation, scale;

	const std::string dt = "Models/Default/COLOR.png";
	const std::string dn = "Models/Default/NRM.png";
	const std::string ds = "Models/Default/SPEC.png";

	ID3D11Buffer*           vertexBuffer = NULL;
	ID3D11Buffer*			indexBuffer = NULL;

	VertexShader*			vertexShader = NULL;
	GeometryShader*			geometryShader = NULL;
	PixelShader*			pixelShader = NULL;

};

#endif